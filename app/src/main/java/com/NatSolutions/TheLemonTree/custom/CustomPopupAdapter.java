package com.NatSolutions.TheLemonTree.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;

import java.util.List;

/**
 * Created by karim on 6/2/18.
 */

public class CustomPopupAdapter extends BaseAdapter {

    private List<?> objects;
    private LayoutInflater inflater;
    private Context context;

    public CustomPopupAdapter(Context context, List<?> objects) {
        this.objects = objects;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.index_custom_popup, null, true);
        TextView itemText = (TextView) rowView.findViewById(R.id.index_textView);
        Typeface font = Typeface.createFromAsset(context.getAssets(),
                "fonts/AlegreyaSansSC-Medium.ttf");
        itemText.setText(objects.get(position).toString());
        itemText.setTypeface(font);
        return rowView;
    }
}