package com.NatSolutions.TheLemonTree.custom;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.databinding.HouseRulesPopupBinding;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karim on 6/2/18.
 */

public class MyDialogFragment extends DialogFragment {

    HouseRulesPopupBinding mFragmentBinding;
    ReservationViewModel mViewModel;
    public List<String> houseRulesList = new ArrayList<>();
    StringBuilder builder = new StringBuilder();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setStyle(STYLE_NO_TITLE, 0);
        mFragmentBinding = HouseRulesPopupBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(ReservationViewModel.class);
        for (String rule : houseRulesList) {
            builder.append(rule);
            builder.append("\n");
        }
        mFragmentBinding.descriptionTextView.setText(builder.toString());
        mFragmentBinding.setVm(mViewModel);
        mFragmentBinding.setView(this);
        return mFragmentBinding.getRoot();
    }

    public View.OnClickListener close() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        };
    }

    public View.OnClickListener next() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                mViewModel.houseRulesNextClick();
            }
        };
    }

    public View.OnClickListener cancel() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        };
    }
}
