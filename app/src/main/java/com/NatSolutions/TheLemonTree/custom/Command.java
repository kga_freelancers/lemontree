package com.NatSolutions.TheLemonTree.custom;


public interface Command {
    void execute();
    void executeWithResult(Object object);
}
