package com.NatSolutions.TheLemonTree.custom;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.utils.App;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by root on 18/05/18.
 */
public class BindingAdapters {

    @BindingAdapter("imageURL")
    public static void bindImageURL(@NonNull ImageView view, @Nullable String url) {
        if (url != null) {
            Glide.with(App.getContext())
                    .load(url)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.main_bg_new)
                            .centerInside()
                    )
                    .into(view);
        }
    }

}
