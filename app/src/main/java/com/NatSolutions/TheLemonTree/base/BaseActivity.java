package com.NatSolutions.TheLemonTree.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.custom.CustomPopupAdapter;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        progressDialog.dismiss();
    }

    protected void showPopUp(int stringId, String rightActionName, String leftActionName,
                             boolean isCancelable, final Command positiveCommand, final Command negativeCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(isCancelable)
                .setMessage(getString(stringId))
                .setPositiveButton(rightActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                })
                .setNegativeButton(leftActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        negativeCommand.execute();
                    }
                }).show();
    }

    protected void showPopUp(String message, String rightActionName, String leftActionName,
                             boolean isCancelable, final Command positiveCommand, final Command negativeCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(isCancelable)
                .setMessage(message)
                .setPositiveButton(rightActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                })
                .setNegativeButton(leftActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        negativeCommand.execute();
                    }
                }).show();

    }

    protected void showPopUp(String string, String positiveActionName,
                             boolean isCancelable) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(isCancelable)
                .setMessage(string)
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    protected void showPopUp(int stringId, String rightActionName,
                             boolean isCancelable, final Command positiveCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(isCancelable)
                .setMessage(getString(stringId))
                .setPositiveButton(rightActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                }).show();
    }


    protected void showPopUp(String string, String rightActionName,
                             boolean isCancelable, final Command positiveCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(isCancelable)
                .setMessage(string)
                .setPositiveButton(rightActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                }).show();
    }

    public void showCustomPopup(List<?> items, final Command command) {
        CustomPopupAdapter adapter = new CustomPopupAdapter(this, items);
        DialogPlus dialogPlus = DialogPlus.newDialog(this)
                .setAdapter(adapter)
                .setCancelable(true)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view,
                                            int position) {
                        command.executeWithResult(item);
                        dialog.dismiss();
                    }
                })
                .setExpanded(true, 500)
                .create();
        dialogPlus.show();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
