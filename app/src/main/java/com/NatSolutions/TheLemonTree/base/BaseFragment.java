package com.NatSolutions.TheLemonTree.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.custom.CustomPopupAdapter;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.util.List;

/**
 * Created by karim on 5/11/18.
 */

public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();
    }

    protected void showProgressDialog(int staringId) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(true);
        progressDialog.setMessage(getString(staringId));
        progressDialog.show();
    }

    protected void hideProgressDialog() {
        progressDialog.dismiss();
    }

    protected void showPopUp(int stringId, String positiveActionName, String negativeActionName,
                             boolean isCancelable, final Command positiveCommand,
                             final Command negativeCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable)
                .setMessage(getString(stringId))
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                })
                .setNegativeButton(negativeActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        negativeCommand.execute();
                    }
                }).show();
    }

    protected void showPopUp(String message, String positiveActionName, String negativeActionName,
                             boolean isCancelable, final Command positiveCommand,
                             final Command negativeCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable)
                .setMessage(message)
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                })
                .setNegativeButton(negativeActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        negativeCommand.execute();
                    }
                }).show();

    }

    protected void showPopUp(int stringId, String positiveActionName,
                             boolean isCancelable, final Command positiveCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable)
                .setMessage(getString(stringId))
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                }).show();
    }

    protected void showPopUp(String string, String positiveActionName,
                             boolean isCancelable, final Command positiveCommand) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable)
                .setMessage(string)
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        positiveCommand.execute();
                    }
                }).show();
    }

    protected void showPopUp(String string, String positiveActionName,
                             boolean isCancelable) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable)
                .setMessage(string)
                .setPositiveButton(positiveActionName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void showCustomPopup(List<?> items, final Command command) {
        CustomPopupAdapter adapter = new CustomPopupAdapter(getActivity(), items);
        DialogPlus dialogPlus = DialogPlus.newDialog(getActivity())
                .setAdapter(adapter)
                .setCancelable(true)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view,
                                            int position) {
                        command.executeWithResult(item);
                        dialog.dismiss();
                    }
                })
                .setExpanded(true,500)
                .create();
        dialogPlus.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
}
