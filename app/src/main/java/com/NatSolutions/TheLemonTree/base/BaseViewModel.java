package com.NatSolutions.TheLemonTree.base;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

/**
 * Created by root on 06/05/18.
 */

public class BaseViewModel extends AndroidViewModel{

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<String> showProgressDialog = new MutableLiveData<>();


    public BaseViewModel(@NonNull Application application) {
        super(application);
    }


}
