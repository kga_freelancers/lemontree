package com.NatSolutions.TheLemonTree.firebase;/*
package com.NatSolutions.TheLemonTree.firebase;

import android.util.Log;


import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

*/

import android.util.Log;

import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Hagar on 6/7/2017.
 */


public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //SharedPreferencesManager.getInstance(getApplicationContext()).setString("token",refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        DataUtil.saveData(this, Constants.FCM_TOKEN, refreshedToken);
        //sendRegistrationToxServer(refreshedToken);
    }


/**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }
}
