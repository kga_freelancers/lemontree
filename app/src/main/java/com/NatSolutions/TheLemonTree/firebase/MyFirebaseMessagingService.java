package com.NatSolutions.TheLemonTree.firebase;/*
package com.NatSolutions.TheLemonTree.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.ui.reservationDetails.ReservationsDetailsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_NOTIFICATION;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_CANCELLED;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_CONFIRMED;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DEPOSIT;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DEPOSITPAID;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DISCONFIRMED;


*/

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.ui.reservationDetails.ReservationsDetailsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_NOTIFICATION;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_CANCELLED;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_CONFIRMED;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DEPOSIT;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DEPOSITPAID;
import static com.NatSolutions.TheLemonTree.utils.Constants.RESERVATION_DISCONFIRMED;

/**
 * Created by Karim on 6/7/2017.
 */


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String NOTIFICATION_CHANNEL_ID = "confirm_channel_id";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null) {
            sendNotification(remoteMessage.getData());
        }
    }

    private void sendNotification(Map<String, String> data) {
        switch (data.get("NotificationType")) {
            case RESERVATION_CONFIRMED:
                int notificationId = 001;
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID, "My Notifications",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription("Channel description");
                    notificationManager.createNotificationChannel(notificationChannel);
                }

                NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                        NOTIFICATION_CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.ic_notification_new)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setAutoCancel(true);


                Intent resultIntent = new Intent(this,
                        ReservationsDetailsActivity.class);
                resultIntent.putExtra("name", data.get("VenueName"));
                resultIntent.putExtra("id", data.get("ReservationId"));
                resultIntent.putExtra("type", data.get("NotificationType"));
                resultIntent.putExtra("promo",data.get("PromoCode"));

                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                builder.setContentIntent(resultPendingIntent);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                builder.setSound(alarmSound);
                builder.setAutoCancel(true);
                notificationManager.notify(notificationId, builder.build());

                break;

            case RESERVATION_DISCONFIRMED:
                int notificationId2 = 002;

                NotificationManager notificationManager2 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID, "My Notifications",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription("Channel description");
                    notificationManager2.createNotificationChannel(notificationChannel);
                }

                NotificationCompat.Builder builder2 = new NotificationCompat.Builder(this,
                        NOTIFICATION_CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.ic_notification_new)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setAutoCancel(true);


                Intent resultIntent2 = new Intent(this,
                        HomeActivity.class);  // ProfileActivity
                resultIntent2.putExtra(EXTRA_NOTIFICATION, EXTRA_NOTIFICATION);
                PendingIntent resultPendingIntent2 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent2,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                builder2.setContentIntent(resultPendingIntent2);
                Uri alarmSound2 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                builder2.setSound(alarmSound2);
                builder2.setAutoCancel(true);
                notificationManager2.notify(notificationId2, builder2.build());

                break;


            case RESERVATION_CANCELLED:
                int notificationId3 = 003;

                NotificationManager notificationManager3 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID, "My Notifications",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription("Channel description");
                    notificationManager3.createNotificationChannel(notificationChannel);
                }

                NotificationCompat.Builder builder3 = new NotificationCompat.Builder(this,
                        NOTIFICATION_CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.ic_notification_new)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setAutoCancel(true);


                Intent resultIntent3 = new Intent(this,
                        HomeActivity.class);  // ProfileActivity
                resultIntent3.putExtra(EXTRA_NOTIFICATION, EXTRA_NOTIFICATION);

                PendingIntent resultPendingIntent3 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent3,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                builder3.setContentIntent(resultPendingIntent3);
                Uri alarmSound3 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                builder3.setSound(alarmSound3);
                builder3.setAutoCancel(true);
                notificationManager3.notify(notificationId3, builder3.build());
                break;


            case RESERVATION_DEPOSIT:
                int notificationId4 = 004;

                NotificationManager notificationManager4 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID, "My Notifications",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription("Channel description");
                    notificationManager4.createNotificationChannel(notificationChannel);
                }

                NotificationCompat.Builder builder4 = new NotificationCompat.Builder(this,
                        NOTIFICATION_CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.ic_notification_new)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setAutoCancel(true);


                Intent resultIntent4 = new Intent(this,
                        ReservationsDetailsActivity.class);
                resultIntent4.putExtra("name", data.get("VenueName"));
                resultIntent4.putExtra("id", data.get("ReservationId"));
                resultIntent4.putExtra("type", data.get("NotificationType"));
                resultIntent4.putExtra("payment_url", data.get("PaymentURL"));

                PendingIntent resultPendingIntent4 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent4,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                builder4.setContentIntent(resultPendingIntent4);
                Uri alarmSound4 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                builder4.setSound(alarmSound4);
                builder4.setAutoCancel(true);
                notificationManager4.notify(notificationId4, builder4.build());
                break;

            case RESERVATION_DEPOSITPAID:
                int notificationId5 = 005;

                NotificationManager notificationManager5 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID, "My Notifications",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription("Channel description");
                    notificationManager5.createNotificationChannel(notificationChannel);
                }

                NotificationCompat.Builder builder5 = new NotificationCompat.Builder(this,
                        NOTIFICATION_CHANNEL_ID)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setSmallIcon(R.drawable.ic_notification_new)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("body"))
                        .setAutoCancel(true);


                Intent resultIntent5 = new Intent(this,
                        HomeActivity.class);  // ProfileActivity
                resultIntent5.putExtra(EXTRA_NOTIFICATION, EXTRA_NOTIFICATION);

                PendingIntent resultPendingIntent5 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent5,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                builder5.setContentIntent(resultPendingIntent5);
                Uri alarmSound5 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                builder5.setSound(alarmSound5);
                builder5.setAutoCancel(true);
                notificationManager5.notify(notificationId5, builder5.build());
                break;

        }
    }

}

