package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 18/05/18.
 */

public class Guest {

    @SerializedName("GuestName")
    public String guestName;
    @SerializedName("GuestPhoneNumber")
    public String guestPhoneNumber="";

}
