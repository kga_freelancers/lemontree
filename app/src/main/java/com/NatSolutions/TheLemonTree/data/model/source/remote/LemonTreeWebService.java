package com.NatSolutions.TheLemonTree.data.model.source.remote;

import com.NatSolutions.TheLemonTree.data.model.CheckReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.CompleteProfileResponse;
import com.NatSolutions.TheLemonTree.data.model.EditUserResponse;
import com.NatSolutions.TheLemonTree.data.model.FavoriteTrackResponse;
import com.NatSolutions.TheLemonTree.data.model.Guest;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationInfoResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.TracksResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.VenueResponse;
import com.NatSolutions.TheLemonTree.data.model.UserTracksResponse;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by karim on 5/11/18.
 */

public interface LemonTreeWebService {

    Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(2, TimeUnit.MINUTES)
            .addInterceptor(new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build();


    @GET("Venue/GetAllVenues")
    Call<VenueResponse> getAllVenues();

    @GET("Booking/CheckReservationBookingDateHasEvent/")
    Call<CheckReservationResponse> checkReservationDate(@Query("date") String date,
                                                        @Query("venueId") String venueID);


    @POST("/api/Booking/BookNewReservation")
    Call<MyResponse> bookReservation(@Header("Content-Type") String type,
                                     @Header("Accept") String accept,
                                     @Query("ClientId") int clientId,
                                     @Query("VenueId") int venueId,
                                     @Query("ReservationTypeId") int reservationTypeId,
                                     @Query("TimeSlotId") int timeSlotId,
                                     @Query("ReservationDate") String date,
                                     @Query("NumberOfPeople") int numberOfPeople,
                                     @Query("Notes") String notes,
                                     @Body List<Guest> guestList);

    @GET("/api/Booking/GetReservationTypeAndTimeSlotsByVenueId")
    Call<ReservationResponse> getReservationData(@Query("venueId") String venueID);

    @GET("/api/Client/GetClientBookingByClientId")
    Call<MyReservationsResponse> getGuestReservations(@Header("Accept") String header,
                                                      @Query("ClientId") int clientId);

    @GET("Client/LoginCLientByFacebookId")
    Call<UserResponse> loginWithFacebook(@Query("ClientFacebookId") String facebookID,
                                         @Query("ClientFCMToken") String firebaseToken);


    @PUT("Client/UpdateClient")
    Call<EditUserResponse> updateUser(@Body User user);

    @POST("Client/RegisterClientByFacebook")
    Call<CompleteProfileResponse> completeProfile(@Body User user);

    @GET("Client/GetClientNotificationsByClientId")
    Call<NotificationsResponse> getUserNotifications(@Query("ClientId") int clientId);


    @GET("Booking/GetReservationInfoById")
    Call<ReservationInfoResponse> getReservationInfo(@Query("ReservationId") int reservationID);

    @GET("Client/GetClientFavouriteMusicByClientId")
    Call<FavoriteTrackResponse> getUserFavouriteMusic(@Query("ClientId") int clientId);

    @POST("Client/AddFavouriteTrack")
    Call<MyResponse> addFavoriteTrack(@Query("TrackId") int trackID, @Query("ClientId") int clientId);

    @GET("Track/GetAllTracks")
    Call<TracksResponse> getAllTracks();

    @DELETE("Client/RemoveFavouriteTrack")
    Call<MyResponse> removeFavoriteTrack(@Query("TrackId") int trackID, @Query("ClientId") int clientId);

    @GET("Client/GetAllTracksByClientId")
    Call<UserTracksResponse> getUserTracks(@Query("ClientId") int clientId);

    @GET("Booking/CheckPaymentStatus")
    Call<MyResponse> checkPaymentStatus(@Query("reservationId") int reservationID,
                                        @Query("payemtAmount") int clientId);

}
