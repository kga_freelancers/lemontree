package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karim on 5/11/18.
 */

public class MyResponse {

    @SerializedName("Message")
    public String message;
    @SerializedName("Status")
    public Boolean status;
}
