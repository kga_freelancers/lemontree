package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amr on 23/09/17.
 */

public class NotificationsResponse {
    @SerializedName("Obj")
    public List<NotificationModel> notificationModelList;

    public class NotificationModel {

        @SerializedName("NotificationMessage")
        public String message;
        @SerializedName("NotificationDate")
        public String date;

        public String getData() {
            return date.substring(0, 10);
        }

    }


}
