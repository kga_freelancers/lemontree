package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 6/2/18.
 */

public class ReservationInfoResponse extends MyResponse {
    @SerializedName("Obj")
    public ReservationInfo reservationInfo;

    public class ReservationInfo {
        @SerializedName("ReservationInfo")
        public List<ReservationInfoData> reservationInfoData;
    }

    public class ReservationInfoData {
        @SerializedName("ReservationDate")
        public String reservationDate;
        @SerializedName("VenueName")
        public String venueName;
        @SerializedName("ReservationTypeName")
        public String reservationType;
        @SerializedName("NumberOfPeople")
        public int numberOfPeople;

        public String getDate() {
            return reservationDate.substring(0, 10);
        }
    }
}
