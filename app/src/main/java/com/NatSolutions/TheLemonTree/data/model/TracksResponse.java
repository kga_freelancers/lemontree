package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amr on 10/11/17.
 */

public class TracksResponse {

    @SerializedName("Obj")
    public Obj response;

    public class Obj {
        @SerializedName("Tracks")
        public List<Track> trackList;
    }


}
