package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 11/06/18.
 */

public class User {

    @SerializedName("ClientAgeRange")
    public String userAgeRange;
    @SerializedName("ClientId")
    public int userID;
    @SerializedName("ClientTypeId")
    public int userTypeID;
    @SerializedName("ClientName")
    public String userName;
    @SerializedName("ClientGender")
    public String userGender;
    @SerializedName("ClientRate")
    public String userRate;
    @SerializedName("ClientAgeRangeId")
    public int userAgeRangeID = 1;
    @SerializedName("ClientBrithdate")
    public String userBirthdate;
    @SerializedName("ClientPhotoPath")
    public String userImage;
    @SerializedName("ClientPhoneNumber")
    public String userPhone;
    @SerializedName("ClientPhoneNumberConfirmed")
    public boolean userPhoneVerified;
    @SerializedName("ClientEmail")
    public String userEmail;
    @SerializedName("ClientEmailConfirmed")
    public boolean userEmailVerified;
    @SerializedName("ClientAddress")
    public String userAddress;
    @SerializedName("ClientFacebookId")
    public String userFacebookID;
    @SerializedName("ClientFacebookProfile")
    public String userFacebookProfileURL;
    @SerializedName("isActive")
    public boolean isActive;
    @SerializedName("ClientFCMToken")
    public String userFCMToken;

    public String returnGender() {
        return userGender.contains("f") ? "Female" : "Male";
    }
}
