package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 5/11/18.
 */

public class VenueResponse extends MyResponse {
    @SerializedName("Obj")
    public Venues venues;


    public class Venues {
        @SerializedName("Venues")
        public List<Venue> venueList;
    }
}
