package com.NatSolutions.TheLemonTree.data.model.source.local;

import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.utils.App;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.google.gson.Gson;

import static com.NatSolutions.TheLemonTree.utils.Constants.USER;

/**
 * Created by karim on 5/26/18.
 */

public class UserDataSource {
    public static void saveUser(User user) {
        DataUtil.saveData(App.getContext(), USER, new Gson().toJson(user));
    }

    public static User getUser() {
        return new Gson().fromJson(DataUtil.getData(App.getContext(), USER, ""),
                User.class);
    }

    public static String getFCMToken() {
        return getUser().userFCMToken;
    }

    public static String getFacebookID() {
        return getUser().userFacebookID;
    }
}
