package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 5/13/18.
 */

public class ReservationResponse extends MyResponse {

    @SerializedName("Obj")
    public ReservationTypes reservationTypes;

    public class ReservationTypes{
        @SerializedName("Types")
        public List<Reservation> reservationList;
    }

    public class Reservation {
        @SerializedName("ReservationTypeName")
        public String reservationType;
        @SerializedName("ReservationTypeId")
        public int reservationTypeID;
        @SerializedName("ReservationTypeTimeSolts")
        public List<ReservationTimeSlot> reservationTimeSlotList;

        @Override
        public String toString() {
            return reservationType;
        }
    }

    public class ReservationTimeSlot {
        @SerializedName("TimeSlotValue")
        public String timeSlotValue;
        @SerializedName("TimeSlotId")
        public int timeSlotID;

        @Override
        public String toString() {
            return timeSlotValue;
        }
    }

}
