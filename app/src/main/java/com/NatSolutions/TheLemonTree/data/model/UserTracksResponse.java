package com.NatSolutions.TheLemonTree.data.model;

import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.Track;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 9/15/18.
 */

public class UserTracksResponse extends MyResponse {
    @SerializedName("Obj")
    public List<Track> tracks;

}
