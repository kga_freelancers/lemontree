package com.NatSolutions.TheLemonTree.data.model.source;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.NatSolutions.TheLemonTree.data.model.FavoriteTrackResponse;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.TracksResponse;
import com.NatSolutions.TheLemonTree.data.model.UserTracksResponse;
import com.NatSolutions.TheLemonTree.data.model.source.remote.LemonTreeWebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by amrahmed on 7/28/18.
 */

public class MusicRepository {

    public MutableLiveData<TracksResponse> tracksListData = new MutableLiveData<>();
    public MutableLiveData<MyResponse> addToFavoriteData = new MutableLiveData<>();
    public MutableLiveData<MyResponse> removeFromFavoriteData = new MutableLiveData<>();
    public MutableLiveData<FavoriteTrackResponse> userFavTracksData = new MutableLiveData<>();
    public MutableLiveData<UserTracksResponse> userTracks = new MutableLiveData<>();

    public LiveData<TracksResponse> loadTracks() {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<TracksResponse> call = lemonTreeWebService.getAllTracks();
        call.enqueue(new Callback<TracksResponse>() {
            @Override
            public void onResponse(Call<TracksResponse> call,
                                   Response<TracksResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    tracksListData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<TracksResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return tracksListData;
    }

    public LiveData<MyResponse> addToFavorite(int trackId, int userId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<MyResponse> call = lemonTreeWebService.addFavoriteTrack(trackId, userId);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call,
                                   Response<MyResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    addToFavoriteData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return addToFavoriteData;
    }

    public LiveData<MyResponse> removeFromFavorite(int trackId, int userId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<MyResponse> call = lemonTreeWebService.removeFavoriteTrack(trackId, userId);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call,
                                   Response<MyResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    removeFromFavoriteData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return removeFromFavoriteData;
    }

    public LiveData<FavoriteTrackResponse> getUserFavoriteTracks(int userId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<FavoriteTrackResponse> call = lemonTreeWebService.getUserFavouriteMusic(userId);
        call.enqueue(new Callback<FavoriteTrackResponse>() {
            @Override
            public void onResponse(Call<FavoriteTrackResponse> call, Response<FavoriteTrackResponse> response) {
                if (response.isSuccessful() && response.body().favoriteTracks != null) {
                    userFavTracksData.setValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<FavoriteTrackResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return userFavTracksData;
    }


    public LiveData<UserTracksResponse> getUserTracks(int userId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<UserTracksResponse> call = lemonTreeWebService.getUserTracks(userId);
        call.enqueue(new Callback<UserTracksResponse>() {
            @Override
            public void onResponse(Call<UserTracksResponse> call, Response<UserTracksResponse> response) {
                if (response.isSuccessful() && response.body() != null &&
                        response.body().tracks != null) {
                    userTracks.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserTracksResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });

        return userTracks;
    }


}
