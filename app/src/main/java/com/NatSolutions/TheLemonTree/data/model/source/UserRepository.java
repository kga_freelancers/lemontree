package com.NatSolutions.TheLemonTree.data.model.source;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.NatSolutions.TheLemonTree.data.model.CompleteProfileResponse;
import com.NatSolutions.TheLemonTree.data.model.EditUserResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.data.model.source.remote.LemonTreeWebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karim on 5/26/18.
 */

public class UserRepository {

    Context mContext;

    public UserRepository(Context context) {
        this.mContext = context;
    }

    public MutableLiveData<UserResponse> userData = new MutableLiveData<>();
    public MutableLiveData<EditUserResponse> updateUserData = new MutableLiveData<>();
    public MutableLiveData<CompleteProfileResponse> completeProfileData = new MutableLiveData<>();

    public LiveData<UserResponse> loginWithFacebook(String fbID, String fcmToken) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<UserResponse> call = lemonTreeWebService.loginWithFacebook(fbID, fcmToken);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                        userData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
        return userData;
    }

    public LiveData<EditUserResponse> editProfile(User user) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<EditUserResponse> call = lemonTreeWebService.updateUser(user);
        call.enqueue(new Callback<EditUserResponse>() {
            @Override
            public void onResponse(Call<EditUserResponse> call, Response<EditUserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().status) {
                        updateUserData.postValue(response.body());
                    }
                }
                else{
                    updateUserData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<EditUserResponse> call, Throwable t) {
                Log.d("error", "error");
                updateUserData.postValue(null);
            }
        });
        return updateUserData;
    }

    public LiveData<CompleteProfileResponse> completeProfile(final User user) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<CompleteProfileResponse> call = lemonTreeWebService.completeProfile(user);
        call.enqueue(new Callback<CompleteProfileResponse>() {
            @Override
            public void onResponse(Call<CompleteProfileResponse> call,
                                   Response<CompleteProfileResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserDataSource.saveUser(user);
                    completeProfileData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<CompleteProfileResponse> call, Throwable t) {
                Log.d("error", "error");
            }
        });
        return completeProfileData;
    }
}
