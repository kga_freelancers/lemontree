package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 26/05/18.
 */

public class MyReservationsResponse extends MyResponse {


    @SerializedName("Obj")
    public Obj obj;


    public class Obj {
        @SerializedName("ClientReservations")
        public List<Reservation> reservationList;
    }

    public class Reservation {

        @SerializedName("ReservationId")
        public int ReservationId;

        @SerializedName("VenueName")
        public String venueName;

        @SerializedName("ReservationDate")
        public String reservationDate;

        @SerializedName("ReservationType")
        public String reservationType;

        @SerializedName("NumberOfPeople")
        public int numberOfPeople;

        @SerializedName("DepositAmount")
        public String depositAmount;

        @SerializedName("DepositPayed")
        public String depositPayed;

        @SerializedName("TimeSlot")
        public String timeSlot;

        @SerializedName("BarcodeSerial")
        public String barcodeSerial;

        @SerializedName("BarcodeImagePath")
        public String barcodeImagePath;

        @SerializedName("Confirmed")
        public String  confirmed;

        @SerializedName("Cancelled")
        public String cancelled;

        @SerializedName("PaymentLink")
        public String paymentURL;

        @SerializedName("PromoCode")
        public String promoCode;


        public String disConfirmedReason;

        public String getStatus() {
            if (cancelled != null && cancelled.contentEquals("true"))
                return "Cancelled";
            else if (confirmed != null && confirmed.contentEquals("true"))
                return "Confirmed";
            return "Pending";
        }

        public String getDate() {
            return reservationDate.substring(0, 10);
        }
    }

}
