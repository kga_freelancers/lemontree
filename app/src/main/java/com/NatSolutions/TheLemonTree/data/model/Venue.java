package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 5/11/18.
 */

public class Venue {
    @SerializedName("VenueName")
    public String venueName;
    @SerializedName("VenueId")
    public String venueID;
    @SerializedName("Roles")
    public List<String> houseRulesList;
    @SerializedName("IsActive")
    public boolean isActive;

    @Override
    public String toString() {
        return venueName;
    }
}
