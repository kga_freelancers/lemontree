package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karim on 6/1/18.
 */

public class CheckReservationResponse extends MyResponse {
    @SerializedName("Obj")
    public CheckReservation checkReservation;

    public class CheckReservation {
        @SerializedName("IsGuestRequired")
        public boolean isGuestList;
    }
}
