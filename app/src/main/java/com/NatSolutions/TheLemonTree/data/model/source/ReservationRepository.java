package com.NatSolutions.TheLemonTree.data.model.source;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.NatSolutions.TheLemonTree.data.model.CheckReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.data.model.GuestListRequest;
import com.NatSolutions.TheLemonTree.data.model.ReservationInfoResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.VenueResponse;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.data.model.source.remote.LemonTreeWebService;
import com.NatSolutions.TheLemonTree.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karim on 5/11/18.
 */

public class ReservationRepository {

    Context mContext;

    public ReservationRepository(Context context) {
        this.mContext = context;
    }

    public MutableLiveData<VenueResponse> venueData = new MutableLiveData<>();
    public MutableLiveData<CheckReservationResponse> checkReservation = new MutableLiveData<>();
    public MutableLiveData<MyResponse> makeReservation = new MutableLiveData<>();
    public MutableLiveData<ReservationResponse> reservationData = new MutableLiveData<>();
    public MutableLiveData<MyReservationsResponse> myReservationsData = new MutableLiveData<>();
    public MutableLiveData<ReservationInfoResponse> reservationInfoData = new MutableLiveData<>();
    public MutableLiveData<NotificationsResponse> notificationsResponseData = new MutableLiveData<>();

    public LiveData<VenueResponse> getAllVenues() {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<VenueResponse> call = lemonTreeWebService.getAllVenues();
        call.enqueue(new Callback<VenueResponse>() {
            @Override
            public void onResponse(Call<VenueResponse> call, Response<VenueResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().status) {
                        venueData.postValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<VenueResponse> call, Throwable t) {

            }
        });
        return venueData;
    }

    public LiveData<ReservationInfoResponse> getReservationInfoData(int reservationID) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<ReservationInfoResponse> call = lemonTreeWebService.getReservationInfo(reservationID);
        call.enqueue(new Callback<ReservationInfoResponse>() {
            @Override
            public void onResponse(Call<ReservationInfoResponse> call,
                                   Response<ReservationInfoResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    reservationInfoData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ReservationInfoResponse> call, Throwable t) {

            }
        });
        return reservationInfoData;
    }

    public LiveData<CheckReservationResponse> checkReservationDate(String date, String venueID) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<CheckReservationResponse> call = lemonTreeWebService.checkReservationDate(date, venueID);
        call.enqueue(new Callback<CheckReservationResponse>() {
            @Override
            public void onResponse(Call<CheckReservationResponse> call,
                                   Response<CheckReservationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    checkReservation.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<CheckReservationResponse> call, Throwable t) {

            }
        });
        return checkReservation;
    }

    public LiveData<ReservationResponse> getReservationData(String venueID) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<ReservationResponse> call = lemonTreeWebService.getReservationData(venueID);
        call.enqueue(new Callback<ReservationResponse>() {
            @Override
            public void onResponse(Call<ReservationResponse> call,
                                   Response<ReservationResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    reservationData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ReservationResponse> call, Throwable t) {

            }
        });
        return reservationData;
    }

    public LiveData<MyResponse> makeReservation(String contentType,
                                                String accept,
                                                int clientID,
                                                int venueID,
                                                int reservationTypeId,
                                                int timeSlotId,
                                                String date,
                                                int numberOfPeople,
                                                String notes,
                                                GuestListRequest guestListRequest) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<MyResponse> call = lemonTreeWebService.bookReservation(contentType,accept,
                clientID, venueID, reservationTypeId, timeSlotId, date, numberOfPeople, notes,
                guestListRequest.guestList);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call,
                                   Response<MyResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    makeReservation.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return makeReservation;
    }

    public LiveData<MyReservationsResponse> getMyReservations(int clientId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<MyReservationsResponse> call = lemonTreeWebService.getGuestReservations
                (Constants.CONTENT_TYPE, clientId);
        call.enqueue(new Callback<MyReservationsResponse>() {
            @Override
            public void onResponse(Call<MyReservationsResponse> call,
                                   Response<MyReservationsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    myReservationsData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MyReservationsResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return myReservationsData;
    }

    public LiveData<NotificationsResponse> loadNotifications(int clientId) {
        LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                create(LemonTreeWebService.class);
        Call<NotificationsResponse> call = lemonTreeWebService.getUserNotifications(clientId);
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call,
                                   Response<NotificationsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    notificationsResponseData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
        return notificationsResponseData;
    }
}
