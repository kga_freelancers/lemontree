package com.NatSolutions.TheLemonTree.data.model;

import com.NatSolutions.TheLemonTree.data.model.EditUserResponse;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 30/05/18.
 */

public class CompleteProfileResponse extends MyResponse{

    @SerializedName("Obj")
    public CompleteProfileResponse.UserInfo userInfo;

    public class UserInfo {
        @SerializedName("ClientId")
        public int clientId;
    }
}
