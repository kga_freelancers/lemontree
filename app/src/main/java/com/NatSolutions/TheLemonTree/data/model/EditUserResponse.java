package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 06/05/18.
 */

public class EditUserResponse extends MyResponse {

    @SerializedName("Obj")
    public UserInfo userInfo;

    public class UserInfo {
        @SerializedName("ClientInfo")
        public List<User> user;
    }

}
