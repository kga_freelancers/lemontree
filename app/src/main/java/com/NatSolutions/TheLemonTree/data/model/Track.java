package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amr on 10/11/17.
 */


public class Track {
    @SerializedName("TrackId")
    public int trackId;
    @SerializedName("TrackName")
    public String trackName;
    @SerializedName("TrackArtist")
    public String trackArtist;
    @SerializedName("TrackDescription")
    public String trckDescription;
    @SerializedName("TrackImagePath")
    public String trackIMage;
    @SerializedName("TrackPath")
    public String trckPath;
    @SerializedName("IsFavourite")
    public boolean isFavourite;

}
