package com.NatSolutions.TheLemonTree.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by karim on 3/3/18.
 */

public class FavoriteTrackResponse {
    @SerializedName("Obj")
    public List<Track> favoriteTracks;
}
