package com.NatSolutions.TheLemonTree.data.model;

/**
 * Created by root on 12/05/18.
 */

public class ViewPagerItemModel {

    public int imageRes;
    public String message;

    public ViewPagerItemModel(int imageRes, String message) {
        this.imageRes = imageRes;
        this.message = message;
    }
}
