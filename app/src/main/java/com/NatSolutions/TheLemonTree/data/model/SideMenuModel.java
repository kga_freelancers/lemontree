package com.NatSolutions.TheLemonTree.data.model;

import android.graphics.drawable.Drawable;

public class SideMenuModel {
    public String name;
    public Drawable image;
    public boolean isFirst;

    public SideMenuModel(String name, Drawable image , boolean isFirst) {
        this.name = name;
        this.image = image;
        this.isFirst = isFirst;
    }
}
