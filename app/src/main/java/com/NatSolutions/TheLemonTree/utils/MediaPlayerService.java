//package com.NatSolutions.TheLemonTree.utils;
//
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.drawable.Drawable;
//import android.os.AsyncTask;
//import android.os.IBinder;
//import android.os.RemoteException;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.media.session.MediaButtonReceiver;
//import android.support.v4.media.session.MediaControllerCompat;
//import android.support.v4.media.session.MediaSessionCompat;
//import android.util.Log;
//
//import com.NatSolutions.TheLemonTree.R;
//import com.NatSolutions.TheLemonTree.data.model.Track;
//import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Target;
//
///**
// * Created by karim on 11/24/17.
// */
////
//public class MediaPlayerService extends Service {
//
//    public static final String ACTION_PLAY = "action_play";
//    public static final String ACTION_PAUSE = "action_pause";
//    public static final String ACTION_REWIND = "action_rewind";
//    public static final String ACTION_FAST_FORWARD = "action_fast_foward";
//    public static final String ACTION_NEXT = "action_next";
//    public static final String ACTION_PREVIOUS = "action_previous";
//    public static final String ACTION_STOP = "action_stop";
//
//    private MediaSessionCompat mSession;
//    private MediaControllerCompat mController;
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    private void handleIntent(Intent intent) {
//        if (intent == null || intent.getAction() == null)
//            return;
//
//        String action = intent.getAction();
//
//        if (action.equalsIgnoreCase(ACTION_PLAY)) {
//            MediaManager.getInstance().play();
//            mController.getTransportControls().play();
//        } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
//            MediaManager.getInstance().pause();
//            mController.getTransportControls().pause();
//        } else if (action.equalsIgnoreCase(ACTION_FAST_FORWARD)) {
//            mController.getTransportControls().fastForward();
//        } else if (action.equalsIgnoreCase(ACTION_REWIND)) {
//            mController.getTransportControls().rewind();
//        } else if (action.equalsIgnoreCase(ACTION_PREVIOUS)) {
//            MediaManager.getInstance().previous();
//            mController.getTransportControls().skipToPrevious();
//        } else if (action.equalsIgnoreCase(ACTION_NEXT)) {
//            MediaManager.getInstance().next();
//            mController.getTransportControls().skipToNext();
//        } else if (action.equalsIgnoreCase(ACTION_STOP)) {
//            mController.getTransportControls().stop();
//        }
//    }
//
//    private NotificationCompat.Action generateAction(int icon, String title, String intentAction) {
//        Intent intent = new Intent(getApplicationContext(), MediaPlayerService.class);
//        intent.setAction(intentAction);
//        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
//        return new NotificationCompat.Action.Builder(icon, title, pendingIntent).build();
//    }
//
//    private void buildNotification(NotificationCompat.Action action) {
//        final Track rbt = MediaManager.getInstance().rbt;
//        if (rbt == null) {
//            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(1);
//            return;
//        }
//        String subtitle = "";
//        if (rbt.trackArtist != null && !rbt.trackArtist.contentEquals(""))
//            subtitle = rbt.trackArtist;
//        else if (rbt.trackName != null)
//            subtitle = rbt.trackName;
//        final int width = (int) getApplicationContext()
//                .getResources()
//                .getDimension(android.R.dimen.notification_large_icon_width);
//
//        final int height = (int) getApplicationContext()
//                .getResources()
//                .getDimension(android.R.dimen.notification_large_icon_height);
//
//
//
//        android.support.v4.media.app.NotificationCompat.MediaStyle style = new
//                android.support.v4.media.app.NotificationCompat.MediaStyle();
//
//        Intent intent = new Intent(getApplicationContext(), MediaPlayerService.class);
//        intent.setAction(ACTION_STOP);
//        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(),
//                1, intent, 0);
//        final NotificationCompat.Builder builder = (NotificationCompat.Builder)
//                new NotificationCompat.Builder(getApplicationContext())
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(rbt.trackName)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
//                        .setContentText(subtitle)
//                        .setDeleteIntent(pendingIntent)
//                        .setStyle(style);
//        if (MediaManager.getInstance().hasPrevious()) {
//            builder.addAction(generateAction(android.R.drawable.ic_media_previous, "Previous", ACTION_PREVIOUS));
//        }
//
//        builder.addAction(action);
//
//        if (MediaManager.getInstance().hasNext()) {
//            builder.addAction(generateAction(android.R.drawable.ic_media_next, "Next", ACTION_NEXT));
//        }
////        style.setShowActionsInCompactView(0, 1, 2, 3, 4);
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, builder.build());
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                Picasso.with(getApplicationContext())
//                        .load(rbt.trackIMage)
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                                builder.setLargeIcon(bitmap);
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                            }
//                        });
//
//                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                notificationManager.notify(1, builder.build());
//
//            }
//        });
//
//
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        try {
//            initMediaSessions();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//
//        handleIntent(intent);
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    private void initMediaSessions() throws RemoteException {
//
//        ComponentName mediaButtonReceiver = new ComponentName(getApplicationContext(), MediaButtonReceiver.class);
//
//
//        mSession = new MediaSessionCompat(getApplicationContext(), "simple player session", mediaButtonReceiver, null);
//        mController = new MediaControllerCompat(getApplicationContext(), mSession.getSessionToken());
//
//        mSession.setCallback(new MediaSessionCompat.Callback() {
//                                 @Override
//                                 public void onPlay() {
//                                     super.onPlay();
//                                     Log.e("MediaPlayerService", "onPlay");
//                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
//                                 }
//
//                                 @Override
//                                 public void onPause() {
//                                     super.onPause();
//                                     Log.e("MediaPlayerService", "onPause");
//                                     buildNotification(generateAction(android.R.drawable.ic_media_play, "Play", ACTION_PLAY));
//                                 }
//
//                                 @Override
//                                 public void onSkipToNext() {
//                                     super.onSkipToNext();
//                                     Log.e("MediaPlayerService", "onSkipToNext");
//                                     //Change media here
//                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
//                                 }
//
//                                 @Override
//                                 public void onSkipToPrevious() {
//                                     super.onSkipToPrevious();
//                                     Log.e("MediaPlayerService", "onSkipToPrevious");
//                                     //Change media here
//                                     buildNotification(generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE));
//                                 }
//
//                                 @Override
//                                 public void onFastForward() {
//                                     super.onFastForward();
//                                     Log.e("MediaPlayerService", "onFastForward");
//                                     //Manipulate current media here
//                                 }
//
//                                 @Override
//                                 public void onRewind() {
//                                     super.onRewind();
//                                     Log.e("MediaPlayerService", "onRewind");
//                                     //Manipulate current media here
//                                 }
//
//                                 @Override
//                                 public void onStop() {
//                                     super.onStop();
//                                     Log.e("MediaPlayerService", "onStop");
//                                     //Stop media player here
//                                     NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                                     notificationManager.cancel(1);
//                                     Intent intent = new Intent(getApplicationContext(), MediaPlayerService.class);
//                                     stopService(intent);
//                                 }
//
//                                 @Override
//                                 public void onSeekTo(long pos) {
//                                     super.onSeekTo(pos);
//                                 }
//                             }
//        );
//    }
//
//    @Override
//    public boolean onUnbind(Intent intent) {
//        mSession.release();
//        return super.onUnbind(intent);
//    }
//
//}
