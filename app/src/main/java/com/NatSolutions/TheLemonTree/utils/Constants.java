package com.NatSolutions.TheLemonTree.utils;

/**
 * Created by karim on 5/11/18.
 */

public class Constants {

    public static String BASE_URL = "http://tlt.net.co/api/";
    public static String EXTRA_VENUE = "extra_venue";
    public static String EXTRA_DATE = "extra_date";
    public static String NUMBER_OF_GUESTS = "number_of_guests";
    public static String CONTENT_TYPE = "application/json";

    public static String ACCEPT = "application/json";
    public static String EXTRA_RESERVATION = "extra_reservation";
    public static String EXTRA_COMPELETE_PROFILE = "extra_compelete_profile";
    public static String EXTRA_OPEN_MY_RESERVATION = "extra_open_my_reservation";

    public static final String RESERVATION_CONFIRMED = "Confirmed";
    public static final String RESERVATION_DISCONFIRMED = "Disconfirmed";
    public static final String RESERVATION_CANCELLED = "Cancelled";
    public static final String RESERVATION_DEPOSIT = "Deposit";
    public static final String RESERVATION_DEPOSITPAID = "DepositPaid";
    public static final String RESERVATION_GENERAL = "General";

    public static final String EXTRA_NOTIFICATION = "extra_notification";
    public static final String USER = "user";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String FB_ID = "fb_id";

    public static final Integer HOUSE_RULES_TYPE = 1;
    public static final Integer PAYMENT_TYPE = 2;

    public static final String KATTAMEYA_MAP_LOCATION
            = "29.995064,31.404775 (The Lemon Tree & Co. Katameya)";
    public static final String MARASSI_MAP_LOCATION =
            "30.977238,28.756542(the lemon tree & co. marassi)";
    public static final String BEACH_BAR_MAP_LOCATION
            = "31.0142256,28.6089154 (the beach bar by The lemon tree & co.)";
    public static final String RITUALS_MAP_LOCATION
            = "31.024097,28.598675 (the rituals by The lemon tree & co.)";

    public static final String EXTRA_TRACK = "extra_track";
    public static final String EXTRA_TRACK_LIST = "extra_track_list";

}
