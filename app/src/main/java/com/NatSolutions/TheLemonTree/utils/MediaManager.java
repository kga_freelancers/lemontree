//package com.NatSolutions.TheLemonTree.utils;
//
//import android.media.MediaPlayer;
//import android.os.AsyncTask;
//import android.os.Handler;
//import android.util.Log;
//
//import com.NatSolutions.TheLemonTree.data.model.Track;
//
//import java.io.IOException;
//import java.util.List;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.ScheduledFuture;
//import java.util.concurrent.TimeUnit;
//
//import wseemann.media.FFmpegMediaMetadataRetriever;
//
///**
// * Created by karim on 11/24/17.
// */
//
//public class MediaManager implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
//
//    private static MediaManager mInstance;
//    private MediaPlayer mMediaPlayer;
//    private OnStateChanged mListener;
//    private String mURL = "";
//    private Handler timer = new Handler();
//    private boolean isPreparing = false;
//    public boolean isBackground = false;
//    public long audioLength;
//    public Track rbt;
//    public List<Track> playlist;
//    private MediaManager() {
//        mMediaPlayer = new MediaPlayer();
//        mMediaPlayer.setOnPreparedListener(this);
//        mMediaPlayer.setOnCompletionListener(this);
//
//    }
//
//    public static MediaManager getInstance() {
//        if (mInstance == null)
//            mInstance = new MediaManager();
//        return mInstance;
//    }
//
//    public void play(Track rbt) {
//        this.rbt = rbt;
//        mURL = rbt.trckPath;
//        mMediaPlayer.stop();
//        mMediaPlayer.reset();
//        try {
//            mMediaPlayer.setDataSource(mURL);
//            isPreparing = true;
//            mMediaPlayer.prepareAsync();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (mListener != null)
//            mListener.onPlayerPlay();
//
//
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
//                    mmr.setDataSource(mURL);
//                    long duration = Long.parseLong(mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION));
//                    mmr.release();
//                    Log.d("Size", "size: " + duration);
//                    MediaManager.this.audioLength = duration;
//                    if (mListener != null)
//                        mListener.onAudioSizeChanged((int) duration);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });
//
//    }
//    public void play() {
//        mMediaPlayer.start();
//        if (mListener != null)
//            mListener.onPlayerPause();
//
//    }
//
//    public void pause() {
//        mMediaPlayer.pause();
//        if (mListener != null)
//            mListener.onPlayerPause();
//
//    }
//
//    public void resume() {
//        mMediaPlayer.start();
//        if (mListener != null) {
//            mListener.onPlayerStart();
//            //mListener.onAudioSizeChanged(mMediaPlayer.getDuration());
//        }
//
//    }
//
//    public boolean isPlaying() {
//        return mMediaPlayer.isPlaying();
//    }
//
//    public void setOnStateChangedListener(OnStateChanged listener) {
//        mListener = listener;
//    }
//
//    public void removeOnStateChangedListener() {
//        mListener = null;
//    }
//
//    @Override
//    public void onPrepared(MediaPlayer mp) {
//        isPreparing = false;
//        mp.start();
//        if (mListener != null)
//            mListener.onPlayerStart();
//    }
//
//
//    @Override
//    public void onCompletion(MediaPlayer mp) {
//        if (!isPreparing && mListener != null)
//            mListener.onPlayerFinish();
//        else if (isBackground) {
////            MusicActivity.playNextInBackground();
//        }
//    }
//
////    public void next() {
////        MusicActivity.playNextInBackground();
////    }
////
////    public void previous() {
////        MusicActivity.playPreviousInBackground();
////    }
//
//    public void seekTo(int progress) {
//        mMediaPlayer.seekTo(progress);
//    }
//
////    public boolean hasPrevious() {
////        return MusicActivity.hasPrevious();
////    }
////
////    public boolean hasNext() {
////        return MusicActivity.hasNext();
////    }
//
//
//    public interface OnStateChanged {
//        void onPlayerPlay();
//
//        void onPlayerStart();
//
//        void onPlayerPause();
//
//        void onPlayerFinish();
//
//        void onAudioSizeChanged(int size);
//
//        void onPlayerProgressChanged(int progress);
//    }
//
//    private final ScheduledExecutorService mExecutorService =
//            Executors.newSingleThreadScheduledExecutor();
//    private ScheduledFuture<?> mScheduleFuture;
//    private final Handler mHandler = new Handler();
//    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
//    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
//    private final Runnable mUpdateProgressTask = new Runnable() {
//        @Override
//        public void run() {
//            updateProgress();
//        }
//    };
//
//    public void scheduleSeekbarUpdate() {
//        stopSeekbarUpdate();
//        if (!mExecutorService.isShutdown()) {
//            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
//                    new Runnable() {
//                        @Override
//                        public void run() {
//                            mHandler.post(mUpdateProgressTask);
//                        }
//                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
//                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
//        }
//    }
//
//    public void stopSeekbarUpdate() {
//        if (mScheduleFuture != null) {
//            mScheduleFuture.cancel(false);
//        }
//    }
//
//    private void updateProgress() {
//        if (mListener != null)
//            mListener.onPlayerProgressChanged(mMediaPlayer.getCurrentPosition());
//    }
//
//    public long getDuration() {
//        if (!mMediaPlayer.isPlaying())
//            return 0;
//        return mMediaPlayer.getDuration();
//    }
//}