package com.NatSolutions.TheLemonTree.ui.home.fragments.notifications;
import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;

/**
 * Created by root on 26/05/18.
 */

public class NotificationItemViewModel {

    public NotificationsResponse.NotificationModel notificationModel;

    public NotificationItemViewModel(NotificationsResponse.NotificationModel notificationModel) {
        this.notificationModel = notificationModel;
    }

}
