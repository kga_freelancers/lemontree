package com.NatSolutions.TheLemonTree.ui.home;

import android.app.Application;
import android.support.annotation.NonNull;
import android.view.View;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.utils.SingleLiveEvent;

/**
 * Created by root on 06/05/18.
 */

public class HomeViewModel extends BaseViewModel {

    private final SingleLiveEvent<Void> mExploreEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mListenEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mReserveEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mReadEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mShopEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mSideMenuEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mOpenEditProfile = new SingleLiveEvent<>();

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }


    public SingleLiveEvent<Void> getExploreEvent() {
        return mExploreEvent;
    }

    public SingleLiveEvent<Void> getListenEvent() {
        return mListenEvent;
    }

    public SingleLiveEvent<Void> getReserveEvent() {
        return mReserveEvent;
    }

    public SingleLiveEvent<Void> getReadEvent() {
        return mReadEvent;
    }

    public SingleLiveEvent<Void> getShopEvent() {
        return mShopEvent;
    }

    public SingleLiveEvent<Void> getEditProfileEvent() {
        return mOpenEditProfile;
    }

    public SingleLiveEvent<Void> getSideMenuEvent() {
        return mSideMenuEvent;
    }

    public View.OnClickListener openExplore() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mExploreEvent.call();
            }
        };
    }

    public View.OnClickListener openListen() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListenEvent.call();
            }
        };
    }

    public View.OnClickListener openReserve() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mReserveEvent.call();
            }
        };
    }

    public View.OnClickListener openRead() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mReadEvent.call();
            }
        };
    }

    public View.OnClickListener openShop() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShopEvent.call();
            }
        };
    }

    public View.OnClickListener openSideMenu() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSideMenuEvent.call();
            }
        };
    }

    public View.OnClickListener openEditProfileActivity() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOpenEditProfile.call();
            }
        };
    }


}
