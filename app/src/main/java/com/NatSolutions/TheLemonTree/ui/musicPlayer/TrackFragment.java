package com.NatSolutions.TheLemonTree.ui.musicPlayer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.Track;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_TRACK;

/**
 * Created by karim on 11/10/17.
 */

public class TrackFragment extends Fragment {

    Track track;

    ImageView headphoneImageView, trackImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track, container, false);
        headphoneImageView = (ImageView) view.findViewById(R.id.headphone_imageView);
        trackImageView = (ImageView) view.findViewById(R.id.track_imageView);
        getBundle();
        return view;
    }

    public static TrackFragment newInstance(Track track) {
        Bundle bundle = new Bundle();
        TrackFragment fragment = new TrackFragment();
        bundle.putString(EXTRA_TRACK, new Gson().toJson(track));
        fragment.setArguments(bundle);
        return fragment;
    }

    private void getBundle() {
        this.track = new Gson().fromJson(getArguments().getString(EXTRA_TRACK), Track.class);
        Glide.with(getActivity())
                .load(track.trackIMage)
                .into(trackImageView);
    }
}
