package com.NatSolutions.TheLemonTree.ui.home.fragments.notifications;

import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;

/**
 * Created by root on 26/05/18.
 */

public interface NotificationCallback {

    void openReservationDetails(MyReservationsResponse.Reservation reservation);
}
