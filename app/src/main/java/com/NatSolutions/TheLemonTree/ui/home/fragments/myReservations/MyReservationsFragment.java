package com.NatSolutions.TheLemonTree.ui.home.fragments.myReservations;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.base.BaseFragment;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.databinding.FragmentMyReservationsBinding;
import com.NatSolutions.TheLemonTree.ui.reservationDetails.ReservationsDetailsActivity;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_RESERVATION;

/**
 * Created by root on 26/05/18.
 */

public class MyReservationsFragment extends BaseFragment {

    FragmentMyReservationsBinding mFragmentBinding;
    MyReservationsViewModel mViewModel;
    MyReservationsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mFragmentBinding = FragmentMyReservationsBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(MyReservationsViewModel.class);
        mFragmentBinding.setVm(mViewModel);
        initRecyclerView();
        setViewObservables();
        return mFragmentBinding.getRoot();
    }

    private void initRecyclerView() {
        adapter = new MyReservationsAdapter(getActivity(), myReservationCallback);
        mFragmentBinding.recyclerView.setHasFixedSize(true);
        mFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentBinding.recyclerView.setAdapter(adapter);
    }

    private void setViewObservables() {

        mViewModel.getMyReservations().observe(getActivity(), new Observer<MyReservationsResponse>() {
            @Override
            public void onChanged(@Nullable MyReservationsResponse myReservationsResponse) {
                adapter.setData(myReservationsResponse.obj.reservationList);
                mFragmentBinding.progressBar.setVisibility(View.GONE);
            }
        });

    }

    MyReservationCallback myReservationCallback = new MyReservationCallback() {
        @Override
        public void openReservationDetails(MyReservationsResponse.Reservation reservation) {
            if (reservation.confirmed != null && reservation.confirmed.contentEquals("true")) {
                if (reservation.cancelled == null) {
                    Intent intent = new Intent(getActivity(), ReservationsDetailsActivity.class);
                    intent.putExtra(EXTRA_RESERVATION, new Gson().toJson(reservation));
                    startActivity(intent);
                }
            } else if (reservation.cancelled == null &&
                    reservation.depositAmount != null &&
                    reservation.depositPayed != null &&
                    reservation.depositPayed.equalsIgnoreCase("false")) {
                Intent intent = new Intent(getActivity(), ReservationsDetailsActivity.class);
                intent.putExtra("id", "" + reservation.ReservationId);
                intent.putExtra("type", Constants.RESERVATION_DEPOSIT);
                if (reservation.paymentURL != null &&
                        !reservation.paymentURL.contentEquals("")) {
                    intent.putExtra("payment_url", reservation.paymentURL);
                } else {
                    intent.putExtra("payment_url", "http://www.thelemontree.co/");
                }
                startActivity(intent);
            }
        }
    };


}
