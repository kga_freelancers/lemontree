package com.NatSolutions.TheLemonTree.ui.explore.canlimon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.CanLimonSliderFragmentBinding;
import com.google.gson.Gson;


/**
 * Created by amr on 21/06/17.
 */

public class CanLimonSliderFragment extends Fragment {

    CanLimonSliderFragmentBinding mFragmentBinding;
    ViewPagerItemModel itemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentBinding = CanLimonSliderFragmentBinding.inflate(inflater, container, false);

        itemModel = new Gson()
                .fromJson(getArguments()
                                .getString("itemModel")
                        , ViewPagerItemModel.class);
        int position = getArguments().getInt("position");

        mFragmentBinding.sliderImageView.setImageResource(itemModel.imageRes);
        setText(position);
        return mFragmentBinding.getRoot();
    }

    private void setText(int position) {

        switch (position) {
            case 1:
            case 2:
                mFragmentBinding.topLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.topTextView.setText(itemModel.message);
                break;
            case 0:
                mFragmentBinding.centerLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.centerTextView.setText(itemModel.message);
                break;
            case 3:
                mFragmentBinding.topCenterLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.topCenterTextView.setText(itemModel.message);

        }
    }

    public static CanLimonSliderFragment newInstance(ViewPagerItemModel model, int itemPosition) {
        CanLimonSliderFragment homeGallerySliderFragment = new CanLimonSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("itemModel", new Gson().toJson(model));
        bundle.putInt("position", itemPosition);
        homeGallerySliderFragment.setArguments(bundle);
        return homeGallerySliderFragment;
    }


}