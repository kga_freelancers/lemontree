package com.NatSolutions.TheLemonTree.ui.musicPlayer;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.Track;
import com.NatSolutions.TheLemonTree.data.model.source.MusicRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcAudioPlayer;
import com.example.jean.jcplayer.JcPlayerView;
import com.example.jean.jcplayer.JcStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import rm.com.audiowave.OnProgressListener;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_TRACK;
import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_TRACK_LIST;

public class MusicPlayerActivity extends AppCompatActivity implements JcPlayerView.OnInvalidPathListener
        , JcPlayerView.JcPlayerViewStatusListener, JcAudioPlayer.controlPLaying, OnProgressListener {

    ViewPager mViewPager;
    TrackPagerAdapter trackPagerAdapter;
    RelativeLayout rootView;
    List<Track> mTrackList = new ArrayList<>();
    Track mTrack;
    private JcPlayerView player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        rootView = findViewById(R.id.rootView);
        player = (JcPlayerView) findViewById(R.id.player);
        player.getInstance(this);
        mTrackList = new Gson().fromJson(getIntent().getStringExtra(EXTRA_TRACK_LIST), new TypeToken<List<Track>>() {
        }.getType());
        mTrack = new Gson().fromJson(getIntent().getStringExtra(EXTRA_TRACK), Track.class);
        setViewPagerData(mTrackList);

        List<JcAudio> jcAudios = new ArrayList<>();
        JcAudio jcAudio;
        for (Track track : mTrackList) {
            if (track.trackArtist != null && !track.trackArtist.contentEquals("")) {
                jcAudio = JcAudio.createFromURL(track.trackName + "\n" +
                                track.trackArtist
                        , track.trckPath);
                jcAudio.setIsFavorite(track.isFavourite);
                jcAudios.add(jcAudio);
            } else {
                jcAudio = JcAudio.createFromURL(track.trackName
                        , track.trckPath);
                jcAudio.setIsFavorite(mTrack.isFavourite);
                jcAudios.add(jcAudio);
            }

        }

        player.initPlaylist(jcAudios);
        player.registerInvalidPathListener(this);
        player.registerStatusListener(this);
        JcAudioPlayer.getInstance().setListener(this);
    }

    private void setViewPagerData(final List<Track> trackList) {
//        int width = getWindowManager().getDefaultDisplay().getWidth();
        trackPagerAdapter = new TrackPagerAdapter(getSupportFragmentManager(), trackList);
        mViewPager.setAdapter(trackPagerAdapter);
//        mViewPager.setClipToPadding(false);
//        mViewPager.setPadding(((width - getResources().getDimensionPixelSize(R.dimen.item_width)) / 2), 0, ((width - getResources().getDimensionPixelSize(R.dimen.item_width)) / 2), 0);

        int idx = -1;
        for (int i = 0; i < trackList.size(); i++) {
            if (trackList.get(i).trackId == mTrack.trackId) {
                idx = i;
                break;
            }
        }
//        mViewPager.setCurrentItem(idx);

        if (mViewPager.getAdapter() != null)
            mViewPager.setAdapter(null);
        mViewPager.setAdapter(trackPagerAdapter);
        final int finalIdx = idx;
        mViewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (finalIdx >= 0)
                    mViewPager.setCurrentItem(finalIdx);
            }
        }, 100);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                player.playAudio(player.getMyPlaylist().get(position));
                mViewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.createNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.kill();
    }

    @Override
    public void onPathError(JcAudio jcAudio) {
        Toast.makeText(this, jcAudio.getPath() + "With problems", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChangedStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {

    }

    private void updateProgress(final JcStatus jcStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float progress = (float) (jcStatus.getDuration() - jcStatus.getCurrentPosition())
                        / (float) jcStatus.getDuration();
                progress = 1.0f - progress;
            }
        });
    }

    @Override
    public void onNextClicked(int trackPosition) {
        player.playAudio(player.getMyPlaylist().get(trackPosition));
        mViewPager.setCurrentItem(trackPosition);
    }

    @Override
    public void onPreviousClicked(int trackPosition) {
        player.playAudio(player.getMyPlaylist().get(trackPosition));
        mViewPager.setCurrentItem(trackPosition);
    }

    @Override
    public void onFavoriteClicked(final int trackPosition) {
        MusicRepository musicRepository = new MusicRepository();
        if (mTrackList.get(trackPosition).isFavourite) {
            musicRepository.removeFromFavorite(mTrackList.get(trackPosition).trackId
                    , UserDataSource.getUser().userID).observe(this, new Observer<MyResponse>() {
                @Override
                public void onChanged(@Nullable MyResponse myResponse) {
//                    SnackbarUtils.showSnackbar(rootView, "added successfully");
                    mTrackList.get(trackPosition).isFavourite = false;
                    player.changeFavorite(false);
                }
            });
        } else {
            musicRepository.addToFavorite(mTrackList.get(trackPosition).trackId
                    , UserDataSource.getUser().userID).observe(this, new Observer<MyResponse>() {
                @Override
                public void onChanged(@Nullable MyResponse myResponse) {
//                    SnackbarUtils.showSnackbar(rootView, "added successfully");
                    mTrackList.get(trackPosition).isFavourite = true;
                    player.changeFavorite(true);
                }
            });
        }
    }

    @Override
    public void onStartTracking(float v) {

    }

    @Override
    public void onStopTracking(float v) {

    }

    @Override
    public void onProgressChanged(float v, boolean b) {

    }

}
