package com.NatSolutions.TheLemonTree.ui.explore.marassi;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityMarassiBinding;

import java.util.ArrayList;
import java.util.List;

public class MarassiActivity extends BaseActivity {

    ActivityMarassiBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_marassi);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getMarassiItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getMarassiItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.marassi_1,
                getString(R.string.marassi_text_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.marassi_2,
                getString(R.string.marassi_text_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.marassi_3,
                getString(R.string.marassi_text_3)));
        return viewPagerItemModelList;
    }

}
