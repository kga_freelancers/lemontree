package com.NatSolutions.TheLemonTree.ui.explore.rituals;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.ui.explore.Katamaya.KatamayaSliderFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by root on 12/03/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private int pagesCount;
    private List<ViewPagerItemModel> itemModels = new ArrayList<>();
    ;

    public ViewPagerAdapter(FragmentManager fm, List<ViewPagerItemModel> list) {
        super(fm);
        this.pagesCount = list.size();
        this.itemModels = list;
    }

    @Override
    public Fragment getItem(int position) {
        return RitualsSliderFragment.newInstance(itemModels.get(position),position);
    }

    @Override
    public int getCount() {
        return pagesCount;
    }


}
