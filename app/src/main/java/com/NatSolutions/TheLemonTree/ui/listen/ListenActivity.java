package com.NatSolutions.TheLemonTree.ui.listen;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.databinding.ActivityListenBinding;
import com.NatSolutions.TheLemonTree.ui.musicList.MusicListActivity;
import com.NatSolutions.TheLemonTree.ui.soon.SoonActivity;

public class ListenActivity extends BaseActivity implements ListenNavigator{

    ActivityListenBinding mActivityBinding;
    ListenViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        setClickListeners();
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_listen);
        mViewModel = ViewModelProviders.of(this).get(ListenViewModel.class);
        mActivityBinding.setVm(mViewModel);
    }


    private void setClickListeners() {
        mActivityBinding.radioTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openRadio();
            }
        });
        mActivityBinding.soundImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMusic();
            }
        });

    }

    @Override
    public void openRadio() {
        startActivity(new Intent(this, SoonActivity.class));
    }

    @Override
    public void openMusic() {
        startActivity(new Intent(this, MusicListActivity.class));
    }
}
