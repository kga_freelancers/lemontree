package com.NatSolutions.TheLemonTree.ui.reservation.fragments;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseFragment;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.data.model.CheckReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.Venue;
import com.NatSolutions.TheLemonTree.data.model.VenueResponse;
import com.NatSolutions.TheLemonTree.databinding.FragmentCheckReservationBinding;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationViewModel;
import com.NatSolutions.TheLemonTree.utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class CheckReservationFragment extends BaseFragment implements
        DatePickerDialog.OnDateSetListener {

    FragmentCheckReservationBinding mFragmentBinding;
    ReservationViewModel mViewModel;
    List<Venue> venueList = new ArrayList<>();
    private Date date;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private Venue venue;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentBinding = FragmentCheckReservationBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(ReservationViewModel.class);
        mFragmentBinding.setVm(mViewModel);
        mFragmentBinding.setView(this);
        subscribeEvents();
        showProgressDialog();
        calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity(),
                this
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return mFragmentBinding.getRoot();
    }

    private void subscribeEvents() {
        mViewModel.getAllVenues().observe(getActivity(), new Observer<VenueResponse>() {
            @Override
            public void onChanged(@Nullable VenueResponse venueResponse) {
                if (!venueResponse.message.contentEquals("")) {
                    showPopUp(venueResponse.message, getString(android.R.string.ok),
                            true, new Command() {
                                @Override
                                public void execute() {
                                }

                                @Override
                                public void executeWithResult(Object object) {

                                }
                            });
                }
                if (venueResponse.venues.venueList != null) {
                    hideProgressDialog();
                    venueList = venueResponse.venues.venueList;
                }
            }
        });


    }

    public View.OnClickListener checkReservationNextClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentBinding.progressBar.setVisibility(View.VISIBLE);
                mViewModel.checkReservationDate().observe(
                        getActivity(),
                        new Observer<CheckReservationResponse>() {
                            @Override
                            public void onChanged(@Nullable CheckReservationResponse
                                                          checkReservationResponse) {
                                mFragmentBinding.progressBar.setVisibility(View.GONE);
                                if (!checkReservationResponse.status) {
                                    if (checkReservationResponse.checkReservation.isGuestList) {
                                        mViewModel.isGuestList.set(true);
                                    } else {
                                        mViewModel.isGuestList.set(false);
                                    }
                                    openMakeReservation();
                                } else {
                                    showPopUp(getString(R.string.please_choose_another_date),
                                            "ok", true);
                                }
                            }
                        });
            }
        };
    }

    public View.OnClickListener showAllVenues() {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showCustomPopup(venueList, new Command() {
                    @Override
                    public void execute() {
                    }

                    @Override
                    public void executeWithResult(Object object) {
                        venue = (Venue) object;
                        mViewModel.setVenue(venue);
                        mViewModel.setVenueHouseRules(venue.houseRulesList);
                        mFragmentBinding.locationTextView.setText(venue.venueName);

                        if (!venue.isActive) {
                            showPopUp(getString(R.string.closed_venue),
                                    "ok", false);
                        }
                    }
                });
            }
        };
    }

    public View.OnClickListener showPicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        };
    }

    public void openMakeReservation() {
        mViewModel.openMakeReservation();
    }


    public static CheckReservationFragment newInstance() {
        CheckReservationFragment fragment = new CheckReservationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        calendar.set(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, i1);
        calendar.set(Calendar.DAY_OF_MONTH, i2);
        mViewModel.dateEvent.setValue("" + i + "-" + (i1 + 1) + "-" + i2);

        mFragmentBinding.dateTextView.setText("" + DateUtils.getMonthName(i1) + " " + i + "," + i2);
    }

}
