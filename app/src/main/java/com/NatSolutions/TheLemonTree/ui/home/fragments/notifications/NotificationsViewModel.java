package com.NatSolutions.TheLemonTree.ui.home.fragments.notifications;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.data.model.source.ReservationRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;

/**
 * Created by root on 26/05/18.
 */

public class NotificationsViewModel extends BaseViewModel {

    private ReservationRepository reservationRepository;


    public NotificationsViewModel(@NonNull Application application) {
        super(application);
        reservationRepository = new ReservationRepository(this.getApplication());
        loadNotifications();
    }

    public LiveData<NotificationsResponse> loadNotifications() {
        return reservationRepository.loadNotifications(UserDataSource.getUser().userID);
    }

}
