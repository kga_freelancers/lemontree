package com.NatSolutions.TheLemonTree.ui.explore.Katamaya;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityKatamayaBinding;

import java.util.ArrayList;
import java.util.List;

public class KatamayaActivity extends BaseActivity {

    ActivityKatamayaBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_katamaya);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getKatamayaItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getKatamayaItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.kattameya_1,
                getString(R.string.katamaya_text_3)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.kattameya_2,
                getString(R.string.katamaya_text_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.kattameya_3,
                getString(R.string.katamaya_text_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.kattameya_4,
                getString(R.string.katamaya_text_1)));
        return viewPagerItemModelList;
    }

}
