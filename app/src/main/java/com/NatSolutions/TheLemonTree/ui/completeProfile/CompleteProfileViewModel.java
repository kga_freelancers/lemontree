package com.NatSolutions.TheLemonTree.ui.completeProfile;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.CompleteProfileResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.source.UserRepository;
import com.NatSolutions.TheLemonTree.utils.App;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.DataUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by root on 06/05/18.
 */

public class CompleteProfileViewModel extends BaseViewModel {

    public ObservableField<String> userNameObservable =
            new ObservableField<>("");
    public ObservableField<String> birthDateObservable =
            new ObservableField<>("");
    public ObservableField<String> genderObservable =
            new ObservableField<>("");
    public ObservableField<String> emailObservable =
            new ObservableField<>("");
    public ObservableField<String> mobileObservable =
            new ObservableField<>("");
    public ObservableField<String> addressObservable =
            new ObservableField<>("");
    public ObservableField<String> userImage =
            new ObservableField<>("");

    public String facebookProfileURL = "";

    private UserRepository userRepository;
    public MutableLiveData<String> showError = new MutableLiveData<>();


    public CompleteProfileViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(this.getApplication());
    }

    public LiveData<CompleteProfileResponse> completeProfile() {
        return userRepository.completeProfile(editUser());
    }

    public LiveData<UserResponse> loginAgain(){
        return userRepository.loginWithFacebook(DataUtil.getData(this.getApplication(),
                Constants.FB_ID,""),DataUtil.getData(this.getApplication(),
                Constants.FCM_TOKEN,""));
    }

    public void fillViewData(JSONObject graphObject) {
        userNameObservable = new ObservableField<>(graphObject.optString("name", ""));
        birthDateObservable = new ObservableField<>(graphObject.optString("birthday", ""));
        genderObservable = new ObservableField<>(graphObject.optString("gender", ""));
        String url = "https://graph.facebook.com/" +
                graphObject.optString("id", "") +
                "/picture?type=large";
        userImage = new ObservableField<>(url);
        try {
            addressObservable = new ObservableField<>(graphObject.getJSONObject
                    ("hometown").optString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        emailObservable = new ObservableField<>(graphObject.optString("email", ""));
        facebookProfileURL = graphObject.optString("link", "");
        if (facebookProfileURL.contentEquals("")) {
            facebookProfileURL = "https://www.facebook.com/app_scoped_user_id/" +
                    graphObject.optString("id", "");
        }
    }

    public boolean validateCheckReservation() {
        boolean isValid = true;
        if (userNameObservable.get().length() == 0) {
            isValid = false;
            showError.setValue("Enter valid username");
        } else if (birthDateObservable.get().length() == 0) {
            isValid = false;
            showError.setValue("Enter valid birthday");
        } else if (genderObservable.get().length() == 0) {
            isValid = false;
            showError.setValue("choose your gender");
        } else if (emailObservable.get().length() == 0) {
            isValid = false;
            showError.setValue("Enter valid email");
        } else if (mobileObservable.get().length() != 11) {
            isValid = false;
            showError.setValue("Enter valid phone number");
        } else if (addressObservable.get().length() == 0) {
            isValid = false;
            showError.setValue("Enter valid address");
        }
        return isValid;
    }


    private User editUser() {
        User user = new User();
        user.userGender = "" + genderObservable.get().charAt(0);
        user.userName = userNameObservable.get();
        user.userBirthdate = birthDateObservable.get();  //         mViewModel.dateEvent.setValue("" + i + "-" + (i1 + 1) + "-" + i2);
        user.userEmail = emailObservable.get();
        user.userPhone = mobileObservable.get();
        user.userImage = userImage.get();
        user.userAddress = addressObservable.get();
        user.userFacebookProfileURL = facebookProfileURL;
        user.userFCMToken = DataUtil.getData(this.getApplication(), Constants.FCM_TOKEN,
                "");
        user.userFacebookID = DataUtil.getData(App.getContext(), Constants.FB_ID, "");
        return user;
    }


}
