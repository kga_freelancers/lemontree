package com.NatSolutions.TheLemonTree.ui.shop;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.databinding.ActivityShopBinding;
import com.NatSolutions.TheLemonTree.ui.explore.boutique.BoutiqueActivity;
import com.NatSolutions.TheLemonTree.ui.explore.canlimon.CanLimonActivity;

public class ShopActivity extends BaseActivity implements ShopNavigator {

    ActivityShopBinding mActivityBinding;
    ShopViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        setClickListeners();
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_shop);
        mViewModel = ViewModelProviders.of(this).get(ShopViewModel.class);
        mActivityBinding.setVm(mViewModel);
    }

    @Override
    public void openBoutique() {
        startActivity(new Intent(this, BoutiqueActivity.class));
    }

    @Override
    public void openCanLimon() {
        startActivity(new Intent(this,
                CanLimonActivity.class));
    }

    private void setClickListeners() {
        mActivityBinding.boutiqueImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBoutique();
            }
        });
        mActivityBinding.canLimonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCanLimon();
            }
        });

    }
}
