package com.NatSolutions.TheLemonTree.ui.reservationDetails;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationInfoResponse;
import com.NatSolutions.TheLemonTree.databinding.ActivityReservationsDetailsBinding;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;

public class ReservationsDetailsActivity extends BaseActivity {

    ActivityReservationsDetailsBinding mActivityBinding;
    ReservationDetailsViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_reservations_details);
        mViewModel = ViewModelProviders.of(this).get(ReservationDetailsViewModel.class);
        mActivityBinding.setView(this);
        mActivityBinding.setVm(mViewModel);
        mActivityBinding.setType(Constants.HOUSE_RULES_TYPE);
        if (getIntent().getStringExtra(Constants.EXTRA_RESERVATION) != null) {
            MyReservationsResponse.Reservation reservation =
                    new Gson().fromJson(getIntent().getStringExtra(Constants.EXTRA_RESERVATION),
                            MyReservationsResponse.Reservation.class);
            fillViewData(reservation);
        }
        checkNotification();
    }

    private void fillViewData(MyReservationsResponse.Reservation reservation) {
        mActivityBinding.dateTextView.setText(reservation.getDate());
        mActivityBinding.placeTextView.setText(reservation.venueName);
        mActivityBinding.typeTextView.setText(reservation.reservationType);
        mActivityBinding.numberTextView.setText(getString(R.string.number_of_people)
                .concat("" + reservation.numberOfPeople));
        mActivityBinding.promoCodeValueTextView.setText("promo code : " + reservation.promoCode);
    }

    public View.OnClickListener viewHouseRules() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHouseRulesDialog();
            }
        };
    }

    public View.OnClickListener payNow() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().getStringExtra("payment_url") != null) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(getIntent().getStringExtra("payment_url")));
                    startActivity(browserIntent);
                }
            }
        };
    }

    private void checkNotification() {
        if (getIntent().getStringExtra("id") != null) {
            if (getIntent().getStringExtra("promo") != null) {
                mActivityBinding.promoCodeValueTextView.
                        setText("promo code : " +
                                getIntent().getStringExtra("promo"));
            }
            int reservationID = Integer.parseInt(getIntent().getStringExtra("id"));
            mViewModel.getReservationInfoData(reservationID).observe(this,
                    new Observer<ReservationInfoResponse>() {
                        @Override
                        public void onChanged(@Nullable ReservationInfoResponse
                                                      reservationInfoResponse) {
                            mActivityBinding.dateTextView.setText(reservationInfoResponse
                                    .reservationInfo.reservationInfoData.get(0).getDate());
                            mActivityBinding.placeTextView.setText(reservationInfoResponse
                                    .reservationInfo.reservationInfoData.get(0).venueName);
                            mActivityBinding.typeTextView.setText(reservationInfoResponse
                                    .reservationInfo.reservationInfoData.get(0).reservationType);
                            mActivityBinding.numberTextView.setText
                                    (getString(R.string.number_of_people).concat(
                                            "" + reservationInfoResponse.reservationInfo
                                                    .reservationInfoData.get(0).numberOfPeople));

                        }
                    });
        }
        if (getIntent().getStringExtra("type") != null) {
            if (getIntent().getStringExtra("type")
                    .contentEquals(Constants.RESERVATION_DEPOSIT)) {
                mActivityBinding.setType(Constants.PAYMENT_TYPE);
            }
        }
    }

    private void showHouseRulesDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.houses_rules_dialog);

        Button dialogButton = (Button) dialog.findViewById(R.id.Agree);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}
