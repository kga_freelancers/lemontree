package com.NatSolutions.TheLemonTree.ui.home;

/**
 * Created by root on 06/05/18.
 */

public interface HomeNavigator {

     void openExplore();
     void openListen();
     void openShop();
     void openRead();
     void openReserve();
}
