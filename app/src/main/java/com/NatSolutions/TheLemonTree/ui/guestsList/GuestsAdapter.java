package com.NatSolutions.TheLemonTree.ui.guestsList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.Guest;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/03/18.
 */

public class GuestsAdapter extends RecyclerView.Adapter<GuestsAdapter.ItemGuestViewHolder> {

    Context mContext;
    List<Guest> guestList = new ArrayList<>();
    private LayoutInflater inflater;

    public GuestsAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public GuestsAdapter.ItemGuestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.item_guest, parent, false);
        return new GuestsAdapter.ItemGuestViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemGuestViewHolder holder, int position) {
        final Guest guest = guestList.get(position);
        if (position == 0) {
            holder.guestNameEditText.setText(UserDataSource.getUser().userName);
            holder.guestNameEditText.setEnabled(false);
        } else {
            holder.guestNameEditText.setEnabled(true);
            holder.guestNameEditText.setHint("Guest " + (position + 1));
        }
        holder.guestNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                guest.guestName = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    @Override
    public int getItemCount() {
        return guestList.size();
    }

    public class ItemGuestViewHolder extends RecyclerView.ViewHolder {
        private EditText guestNameEditText;

        public ItemGuestViewHolder(View view) {
            super(view);
            guestNameEditText = (EditText) view.findViewById(R.id.guest_name_editText);
        }
    }

    public void setData(List<Guest> guestList) {
        this.guestList = guestList;
        notifyDataSetChanged();
    }

}
