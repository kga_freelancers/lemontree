package com.NatSolutions.TheLemonTree.ui.soon;

import android.app.Application;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;

/**
 * Created by karim on 5/17/18.
 */

public class SoonViewModel extends BaseViewModel {
    public SoonViewModel(@NonNull Application application) {
        super(application);
    }
}
