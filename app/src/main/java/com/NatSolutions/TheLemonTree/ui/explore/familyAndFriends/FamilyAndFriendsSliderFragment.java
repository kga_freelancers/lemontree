package com.NatSolutions.TheLemonTree.ui.explore.familyAndFriends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.FamilyAndFriendsSliderFragmentBinding;
import com.google.gson.Gson;


/**
 * Created by amr on 21/06/17.
 */

public class FamilyAndFriendsSliderFragment extends Fragment {

    FamilyAndFriendsSliderFragmentBinding mFragmentBinding;
    ViewPagerItemModel itemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentBinding = FamilyAndFriendsSliderFragmentBinding.inflate(inflater, container, false);

        itemModel = new Gson()
                .fromJson(getArguments()
                                .getString("itemModel")
                        , ViewPagerItemModel.class);
        int position = getArguments().getInt("position");

        mFragmentBinding.sliderImageView.setImageResource(itemModel.imageRes);
        setText(position);
        return mFragmentBinding.getRoot();
    }

    private void setText(int position) {

        switch (position) {
            case 0:
                mFragmentBinding.bottomLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.bottomTextView.setText(itemModel.message);
                break;
            case 1:
                mFragmentBinding.centerLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.centerTextView.setText(itemModel.message);
                break;
            case 2:
                mFragmentBinding.topLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.topTextView.setText(itemModel.message);
                break;

        }
    }

    public static FamilyAndFriendsSliderFragment newInstance(ViewPagerItemModel model, int itemPosition) {
        FamilyAndFriendsSliderFragment homeGallerySliderFragment = new FamilyAndFriendsSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("itemModel", new Gson().toJson(model));
        bundle.putInt("position", itemPosition);
        homeGallerySliderFragment.setArguments(bundle);
        return homeGallerySliderFragment;
    }


}