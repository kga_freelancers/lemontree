package com.NatSolutions.TheLemonTree.ui.listen;

/**
 * Created by karim on 5/31/18.
 */

public interface ListenNavigator {
    void openRadio();
    void openMusic();
}
