package com.NatSolutions.TheLemonTree.ui.musicList;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.UserTracksResponse;
import com.NatSolutions.TheLemonTree.data.model.source.MusicRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;

public class MusicListActivity extends BaseActivity {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private MusicListAdapter musicListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        initializeRecyclerView();
        getAllTracks();
    }

    private void initializeRecyclerView() {
        musicListAdapter = new MusicListAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(musicListAdapter);
    }

    private void getAllTracks() {
        progressBar.setVisibility(View.VISIBLE);
        MusicRepository repo = new MusicRepository();

        repo.getUserTracks(UserDataSource.getUser().userID)
                .observe(this, new Observer<UserTracksResponse>() {
                    @Override
                    public void onChanged(@Nullable UserTracksResponse userTracksResponse) {
                        musicListAdapter.setData(userTracksResponse.tracks);
                        progressBar.setVisibility(View.GONE);
                    }
                });


    }
}
