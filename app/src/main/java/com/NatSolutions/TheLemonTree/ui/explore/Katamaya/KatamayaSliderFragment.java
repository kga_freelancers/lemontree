package com.NatSolutions.TheLemonTree.ui.explore.Katamaya;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.KatamyaSliderFragmentBinding;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationActivity;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;


/**
 * Created by amr on 21/06/17.
 */

public class KatamayaSliderFragment extends Fragment {

    KatamyaSliderFragmentBinding mFragmentBinding;
    ViewPagerItemModel itemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentBinding = KatamyaSliderFragmentBinding.inflate(inflater, container, false);

        itemModel = new Gson()
                .fromJson(getArguments()
                                .getString("itemModel")
                        , ViewPagerItemModel.class);
        int position = getArguments().getInt("position");

        mFragmentBinding.setView(this);
        mFragmentBinding.sliderImageView.setImageResource(itemModel.imageRes);
        setText(position);
        return mFragmentBinding.getRoot();
    }

    private void setText(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                mFragmentBinding.bottomLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.bottomTextView.setText(itemModel.message);
                break;
            case 3:
                mFragmentBinding.locationButton.setVisibility(View.VISIBLE);
                mFragmentBinding.reserveButton.setVisibility(View.VISIBLE);
        }
    }

    public static KatamayaSliderFragment newInstance(ViewPagerItemModel model, int itemPosition) {
        KatamayaSliderFragment homeGallerySliderFragment = new KatamayaSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("itemModel", new Gson().toJson(model));
        bundle.putInt("position", itemPosition);
        homeGallerySliderFragment.setArguments(bundle);
        return homeGallerySliderFragment;
    }

    public View.OnClickListener openLocation() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=loc:" +
                                Constants.KATTAMEYA_MAP_LOCATION));
                startActivity(intent);
            }
        };
    }

    public View.OnClickListener openReservation() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ReservationActivity.class));
            }
        };
    }

}