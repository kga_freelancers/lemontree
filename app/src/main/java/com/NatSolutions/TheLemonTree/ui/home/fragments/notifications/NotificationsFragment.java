package com.NatSolutions.TheLemonTree.ui.home.fragments.notifications;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.databinding.FragmentNotificationsBinding;

/**
 * Created by root on 01/06/18.
 */

public class NotificationsFragment extends Fragment {


    FragmentNotificationsBinding mFragmentBinding;
    NotificationsViewModel mViewModel;
    NotificationsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentBinding = FragmentNotificationsBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(NotificationsViewModel.class);
        mFragmentBinding.setVm(mViewModel);
        initRecyclerView();
        setViewObservables();
        return mFragmentBinding.getRoot();
    }

    private void initRecyclerView() {
        adapter = new NotificationsAdapter(getActivity());
        mFragmentBinding.recyclerView.setHasFixedSize(true);
        mFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFragmentBinding.recyclerView.setAdapter(adapter);
    }

    private void setViewObservables() {

        mViewModel.loadNotifications().observe(getActivity(), new Observer<NotificationsResponse>() {
            @Override
            public void onChanged(@Nullable NotificationsResponse notificationsResponse) {
                adapter.setData(notificationsResponse.notificationModelList);
                mFragmentBinding.progressBar.setVisibility(View.GONE);
            }
        });

    }

}
