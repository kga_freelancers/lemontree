package com.NatSolutions.TheLemonTree.ui.editProfile;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.data.model.EditUserResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.databinding.ActivityEditProfileBinding;
import com.NatSolutions.TheLemonTree.utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditProfileActivity extends BaseActivity implements EditProfileNavigator,
        DatePickerDialog.OnDateSetListener {

    ActivityEditProfileBinding mActivityBinding;
    EditProfileViewModel mViewModel;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        subscribeToNavigationChanges();
        calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this,
                this
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_edit_profile);
        mViewModel = ViewModelProviders.of(this).get(EditProfileViewModel.class);
        mActivityBinding.setVm(mViewModel);
        mActivityBinding.setUser(UserDataSource.getUser());
        mActivityBinding.setView(this);
    }

    public View.OnClickListener showPicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        };
    }

    public View.OnClickListener showGenderList() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCustomPopup(getGenderTypes(), new Command() {
                    @Override
                    public void execute() {

                    }

                    @Override
                    public void executeWithResult(Object object) {
                        mActivityBinding.genderEdit.setText(object.toString());
                    }
                });
            }
        };
    }

    private void subscribeToNavigationChanges() {

    }

    public View.OnClickListener updateUser() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivityBinding.progressBar.setVisibility(View.VISIBLE);
                mViewModel.updateUser().observe(EditProfileActivity.this,
                        new Observer<EditUserResponse>() {
                            @Override
                            public void onChanged(@Nullable EditUserResponse editUserResponse) {
                                mActivityBinding.progressBar.setVisibility(View.GONE);
                                if (editUserResponse != null && editUserResponse.status &&
                                        editUserResponse.message.contentEquals("")) {
                                    User user = UserDataSource.getUser();
                                    user.userName = editUserResponse.userInfo.user.get(0).userName;
                                    user.userBirthdate = editUserResponse.userInfo.user.get(0)
                                            .userBirthdate;
                                    user.userPhone = editUserResponse.userInfo.user.get(0)
                                            .userPhone;
                                    user.userEmail = editUserResponse.userInfo.user.get(0)
                                            .userEmail;
                                    UserDataSource.saveUser(user);
                                    finish();
                                } else {
                                    showPopUp(getString(R.string.something_went_wrong),
                                            "ok", true);
                                }
                            }
                        });
            }
        };
    }


    private List<String> getGenderTypes() {
        List<String> genderList = new ArrayList<>();
        genderList.add("male");
        genderList.add("female");
        return genderList;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        calendar.set(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, i1);
        calendar.set(Calendar.DAY_OF_MONTH, i2);
        mActivityBinding.birthEdit.setText("" + DateUtils.getMonthName(i1) + " " + i + "," + i2);
    }
}
