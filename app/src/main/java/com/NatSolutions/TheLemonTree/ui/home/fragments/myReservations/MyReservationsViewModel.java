package com.NatSolutions.TheLemonTree.ui.home.fragments.myReservations;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.data.model.source.ReservationRepository;
import com.NatSolutions.TheLemonTree.data.model.source.UserRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;

/**
 * Created by root on 26/05/18.
 */

public class MyReservationsViewModel extends BaseViewModel {

    private ReservationRepository reservationRepository;


    public MyReservationsViewModel(@NonNull Application application) {
        super(application);
        reservationRepository = new ReservationRepository(this.getApplication());
        getMyReservations();
    }

    public LiveData<MyReservationsResponse> getMyReservations() {
        return reservationRepository.getMyReservations(UserDataSource.getUser().userID);
    }

}
