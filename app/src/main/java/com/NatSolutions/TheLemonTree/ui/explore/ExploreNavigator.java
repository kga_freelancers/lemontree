package com.NatSolutions.TheLemonTree.ui.explore;

/**
 * Created by root on 12/05/18.
 */

public interface ExploreNavigator {

    void openKatamaya();
    void openBeachBar();
    void openCanLimon();
    void openFamilyAndFriends();
    void openMarassi();
    void openBoutique();
    void openRituals();
}
