package com.NatSolutions.TheLemonTree.ui.home.fragments.notifications;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.databinding.IndexNotificationBinding;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/03/18.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder> {

    Context mContext;
    List<NotificationsResponse.NotificationModel> notificationModelList = new ArrayList<>();

    public NotificationsAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        IndexNotificationBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.index_notification, parent, false);

        return new NotificationViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        final NotificationsResponse.NotificationModel notificationModel = notificationModelList.get(position);
        holder.bind(new NotificationItemViewModel(notificationModel));
        if (position % 2 == 0) {
            holder.binding.bgImageView.setBackgroundResource(R.drawable.bg_left_reservation);
        } else {
            holder.binding.bgImageView.setBackgroundResource(R.drawable.bg_right_reservations);
        }

    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        private IndexNotificationBinding binding;

        public NotificationViewHolder(IndexNotificationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(NotificationItemViewModel itemViewModle) {
            binding.setVm(itemViewModle);
            binding.executePendingBindings();
        }
    }

    public void setData(List<NotificationsResponse.NotificationModel> notificationModelList) {
        this.notificationModelList = notificationModelList;
        notifyDataSetChanged();
    }

}
