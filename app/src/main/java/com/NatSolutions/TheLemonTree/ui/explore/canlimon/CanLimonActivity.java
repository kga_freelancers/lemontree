package com.NatSolutions.TheLemonTree.ui.explore.canlimon;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityCanLimonBinding;

import java.util.ArrayList;
import java.util.List;

public class CanLimonActivity extends BaseActivity {

    ActivityCanLimonBinding mActivityBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_can_limon);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
       ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getBechBarItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getBechBarItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.can_limon_1,
                getString(R.string.canlimon_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.can_limon_2,
                getString(R.string.canlimon_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.can_limon_3,
                getString(R.string.canlimon_3)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.can_limon_4,
                getString(R.string.canlimon_4)));

        return viewPagerItemModelList;
    }
}
