package com.NatSolutions.TheLemonTree.ui.login;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.view.View;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.source.UserRepository;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.NatSolutions.TheLemonTree.utils.SingleLiveEvent;

/**
 * Created by root on 06/05/18.
 */

public class LoginViewModel extends BaseViewModel {

    private UserRepository userRepository;
    private final SingleLiveEvent<Void> facebookClickEvent = new SingleLiveEvent<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(this.getApplication());
    }

    public View.OnClickListener facebookClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookClickEvent.call();
            }
        };
    }

    public SingleLiveEvent<Void> getFacebookClickEvent() {
        return facebookClickEvent;
    }

    public LiveData<UserResponse> loginWithFacebook() {
        return userRepository.loginWithFacebook(DataUtil.getData(
                this.getApplication(),Constants.FB_ID,""),
                DataUtil.getData(this.getApplication(),Constants.FCM_TOKEN,""));
    }

}
