package com.NatSolutions.TheLemonTree.ui.explore;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.databinding.ActivityExplore2Binding;
import com.NatSolutions.TheLemonTree.databinding.ActivityExploreBinding;
import com.NatSolutions.TheLemonTree.ui.explore.Katamaya.KatamayaActivity;
import com.NatSolutions.TheLemonTree.ui.explore.bechBar.BeachBarActivity;
import com.NatSolutions.TheLemonTree.ui.explore.boutique.BoutiqueActivity;
import com.NatSolutions.TheLemonTree.ui.explore.canlimon.CanLimonActivity;
import com.NatSolutions.TheLemonTree.ui.explore.familyAndFriends.FamilyAndFriendsActivity;
import com.NatSolutions.TheLemonTree.ui.explore.marassi.MarassiActivity;
import com.NatSolutions.TheLemonTree.ui.explore.rituals.RitualsActivity;

public class ExploreActivity extends BaseActivity implements ExploreNavigator {

    ActivityExplore2Binding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_explore_2);
        setClickListeners();
    }

    @Override
    public void openKatamaya() {
        startActivity(new Intent(this, KatamayaActivity.class));
    }

    @Override
    public void openBeachBar() {
        startActivity(new Intent(this, BeachBarActivity.class));
    }

    @Override
    public void openCanLimon() {
        startActivity(new Intent(this, CanLimonActivity.class));
    }

    @Override
    public void openFamilyAndFriends() {
        startActivity(new Intent(this, FamilyAndFriendsActivity.class));
    }

    @Override
    public void openMarassi() {
        startActivity(new Intent(this, MarassiActivity.class));
    }

    @Override
    public void openBoutique() {
        startActivity(new Intent(this, BoutiqueActivity.class));
    }

    @Override
    public void openRituals() {
        startActivity(new Intent(this, RitualsActivity.class));
    }

    private void setClickListeners() {
        mActivityBinding.katamyaImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openKatamaya();
            }
        });

        mActivityBinding.bechBarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBeachBar();
            }
        });

      /*  mActivityBinding.canLimonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCanLimon();
            }
        });*/

        mActivityBinding.familyAndFriendsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFamilyAndFriends();
            }
        });

        mActivityBinding.marassiImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMarassi();
            }
        });
        mActivityBinding.ritulasImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openRituals();
            }
        });

       /* mActivityBinding.boutiqueImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBoutique();
            }
        });*/

    }

}
