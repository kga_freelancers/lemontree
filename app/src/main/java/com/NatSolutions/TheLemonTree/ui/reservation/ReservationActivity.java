package com.NatSolutions.TheLemonTree.ui.reservation;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.Venue;
import com.NatSolutions.TheLemonTree.databinding.ActivityReservationBinding;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.ui.reservation.fragments.CheckReservationFragment;
import com.NatSolutions.TheLemonTree.ui.reservation.fragments.MakeReservationFragment;

public class ReservationActivity extends BaseActivity implements ReservationNavigator {

    ActivityReservationBinding mActivityBinding;
    ReservationViewModel mViewModel;
    private Venue venue;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        openCheckReservation();
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_reservation);
        mViewModel = ViewModelProviders.of(this).get(ReservationViewModel.class);
        mActivityBinding.setVm(mViewModel);
        subscribeEvents();
    }


    @Override
    public void openCheckReservation() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CheckReservationFragment.newInstance())
                .commit();
    }

    @Override
    public void openMakeReservation(Venue venue, String date) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, MakeReservationFragment.newInstance(venue, date))
                .commit();
    }

    private void subscribeEvents() {
        mViewModel.getMakeReservationEvent().observe(this, new Observer() {
            @Override
            public void onChanged(@Nullable Object o) {
                openMakeReservation(ReservationActivity.this.venue,
                        ReservationActivity.this.date);
            }
        });
        mViewModel.venueEvent.observe(this, new Observer<Venue>() {
            @Override
            public void onChanged(@Nullable Venue venue) {
                ReservationActivity.this.venue = venue;
            }
        });
        mViewModel.dateEvent.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                ReservationActivity.this.date = s;
            }
        });
    }

    @Override
    public void openHome() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
