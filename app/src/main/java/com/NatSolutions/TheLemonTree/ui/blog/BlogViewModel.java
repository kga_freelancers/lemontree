package com.NatSolutions.TheLemonTree.ui.blog;

import android.app.Application;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;

/**
 * Created by karim on 5/17/18.
 */

public class BlogViewModel extends BaseViewModel {
    public BlogViewModel(@NonNull Application application) {
        super(application);
    }
}
