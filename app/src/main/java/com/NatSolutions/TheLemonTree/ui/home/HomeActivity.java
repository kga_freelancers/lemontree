package com.NatSolutions.TheLemonTree.ui.home;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.databinding.ActivityHomeBinding;
import com.NatSolutions.TheLemonTree.ui.editProfile.EditProfileActivity;
import com.NatSolutions.TheLemonTree.ui.home.fragments.about.AboutFragment;
import com.NatSolutions.TheLemonTree.ui.home.fragments.home.HomeFragment;
import com.NatSolutions.TheLemonTree.ui.home.fragments.myMusic.MyMusicFragment;
import com.NatSolutions.TheLemonTree.ui.home.fragments.myReservations.MyReservationsFragment;
import com.NatSolutions.TheLemonTree.ui.home.fragments.notifications.NotificationsFragment;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.Utils;

public class HomeActivity extends BaseActivity {

    ActivityHomeBinding mActivityBinding;
    HomeViewModel mViewModel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        openHomeFragment();
        subscribeToNavigationChanges();
        initializeRecyclerView();
        checkNotification();
    }


    private void checkNotification() {
        if (getIntent().getStringExtra(Constants.EXTRA_NOTIFICATION) != null ||
                getIntent().getStringExtra(Constants.EXTRA_OPEN_MY_RESERVATION) != null) {
            openReservation();
        }
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        mActivityBinding.setVm(mViewModel);
        setUserData();
    }

    private void subscribeToNavigationChanges() {
        mViewModel.getSideMenuEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                if (!mActivityBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mActivityBinding.drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    closeDrawer();
                }
            }
        });

        mViewModel.getEditProfileEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                openEditProfile();
            }
        });
    }

    private void setUserData() {
        if (UserDataSource.getUser() != null) {
            mActivityBinding.header.setUser(UserDataSource.getUser());
        }
        mActivityBinding.header.headerLayout.setOnClickListener(
                mViewModel.openEditProfileActivity());
        mActivityBinding.facebookImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openFacebook(HomeActivity.this);
            }

        });

        mActivityBinding.instagramImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openInstagram(HomeActivity.this);
            }
        });
    }

    private void initializeRecyclerView() {
        mActivityBinding.recyclerView.setHasFixedSize(true);
        mActivityBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mActivityBinding.recyclerView.setAdapter(
                new SideMenuAdapter(this, mSideMenuNavigator));
    }


    private void openHomeFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();
    }

    private void openReservation() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MyReservationsFragment())
                .addToBackStack("my_fragment")
                .commit();
    }

    private void openNotifications() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new NotificationsFragment())
                .addToBackStack("my_fragment")
                .commit();
    }

    private void openAboutUs() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new AboutFragment())
                .addToBackStack("about")
                .commit();
    }

    private void openMyMusic() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MyMusicFragment())
                .addToBackStack("My Music")
                .commit();
    }
    SideMenuNavigator mSideMenuNavigator = new SideMenuNavigator() {
        @Override
        public void sideMenuItemsNavigator(int position) {
            switch (position) {
                case 0:
                    openNotifications();
                    closeDrawer();
                    break;
                case 1:
                    openReservation();
                    closeDrawer();
                    break;
                case 2:
                    openMyMusic();
                    closeDrawer();
                    break;
                case 3:
                    openAboutUs();
                    closeDrawer();
                    break;

            }
        }
    };

    private void closeDrawer() {
        mActivityBinding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void openEditProfile() {
        startActivity(new Intent(this,
                EditProfileActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (mActivityBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
            mActivityBinding.drawerLayout.closeDrawer(GravityCompat.START);
        else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UserDataSource.getUser() != null)
            mActivityBinding.header.userMobileTextView.setText(UserDataSource.getUser().userPhone);
    }
}
