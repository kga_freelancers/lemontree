package com.NatSolutions.TheLemonTree.ui.completeProfile;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.data.model.CompleteProfileResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.databinding.ActivityCompleteProfileBinding;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.utils.DateUtils;
import com.NatSolutions.TheLemonTree.utils.SnackbarUtils;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_COMPELETE_PROFILE;

public class CompleteProfileActivity extends BaseActivity implements
        CompleteProfileNavigator, DatePickerDialog.OnDateSetListener {

    ActivityCompleteProfileBinding mActivityBinding;
    CompleteProfileViewModel mViewModel;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
        JSONObject graphObject = new Gson()
                .fromJson(getIntent()
                        .getStringExtra(EXTRA_COMPELETE_PROFILE), JSONObject.class);
        calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this,
                this
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        mViewModel.fillViewData(graphObject);
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_complete_profile);
        mViewModel = ViewModelProviders.of(this).get(CompleteProfileViewModel.class);
        mActivityBinding.setVm(mViewModel);
        mActivityBinding.setUser(UserDataSource.getUser());
        mActivityBinding.setView(this);

        mViewModel.showError.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                SnackbarUtils.showSnackbar(mActivityBinding.rootView, s);
            }
        });
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        calendar.set(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, i1);
        calendar.set(Calendar.DAY_OF_MONTH, i2);
        mActivityBinding.birthEdit.setText("" + DateUtils.getMonthName(i1) + " " + i + "," + i2);
        mViewModel.birthDateObservable.set("" + i + "-" + (i1 + 1) + "-" + i2);
    }

    public View.OnClickListener showGenderList() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomPopup(getGenderTypes(), new Command() {
                    @Override
                    public void execute() {

                    }

                    @Override
                    public void executeWithResult(Object object) {
                        mActivityBinding.genderEdit.setText(object.toString());
                    }
                });
            }
        };
    }


    public View.OnClickListener showPicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        };
    }

    private List<String> getGenderTypes() {
        List<String> genderList = new ArrayList<>();
        genderList.add("male");
        genderList.add("female");
        return genderList;
    }

    public View.OnClickListener updateUser() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mViewModel.validateCheckReservation()) return;
                mActivityBinding.progressBar.setVisibility(View.VISIBLE);
                mViewModel.completeProfile().observe(CompleteProfileActivity.this,
                        new Observer<CompleteProfileResponse>() {
                            @Override
                            public void onChanged(@Nullable CompleteProfileResponse completeProfileResponse) {
                                if (completeProfileResponse.status &&
                                        completeProfileResponse.message.contentEquals("")) {
                                    mViewModel.loginAgain().observe(
                                            CompleteProfileActivity.this,
                                            new Observer<UserResponse>() {
                                                @Override
                                                public void onChanged
                                                        (@Nullable UserResponse userResponse) {
                                                    mActivityBinding.progressBar
                                                            .setVisibility(View.GONE);
                                                    if (userResponse.status &&
                                                            userResponse.message
                                                                    .contentEquals("")) {
                                                        User user = UserDataSource.getUser();
                                                        user.userID = userResponse.user.userID;
                                                        UserDataSource.saveUser(user);
                                                        openHome();
                                                    } else if (userResponse.message.length() > 0) {
                                                        SnackbarUtils.showSnackbar(mActivityBinding
                                                                        .rootView,
                                                                userResponse.message);
                                                    } else {
                                                        showPopUp(getString(R.string.something_went_wrong),
                                                                "ok", true);
                                                    }
                                                }
                                            });
                                } else if (completeProfileResponse.message.length() > 0) {
                                    SnackbarUtils.showSnackbar(mActivityBinding
                                                    .rootView,
                                            completeProfileResponse.message);
                                } else {
                                    showPopUp(getString(R.string.something_went_wrong),
                                            "ok", true);
                                }
                            }
                        });

            }
        };
    }

    private void openHome() {
        startActivity(new Intent(this, HomeActivity.class));

    }


}
