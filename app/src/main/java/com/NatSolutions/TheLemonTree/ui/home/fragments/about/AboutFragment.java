package com.NatSolutions.TheLemonTree.ui.home.fragments.about;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.databinding.FragmentAboutBinding;

/**
 * Created by root on 26/05/18.
 */

public class AboutFragment extends Fragment {

    FragmentAboutBinding mFragmentBinding;
    AboutViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mFragmentBinding = FragmentAboutBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(AboutViewModel.class);
        mFragmentBinding.setVm(mViewModel);
        return mFragmentBinding.getRoot();
    }

    public static AboutFragment newInstance() {
        Bundle args = new Bundle();
        AboutFragment fragment = new AboutFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
