package com.NatSolutions.TheLemonTree.ui.explore.rituals;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityRitualsBinding;

import java.util.ArrayList;
import java.util.List;

public class RitualsActivity extends BaseActivity {

    ActivityRitualsBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_rituals);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getRitualsItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getRitualsItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.rituals_1,
                getString(R.string.rituals_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.rituals_2,
                getString(R.string.rituals_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.rituals_3,
                getString(R.string.rituals_3)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.rituals_4,
                getString(R.string.rituals_4)));
        return viewPagerItemModelList;
    }

}
