package com.NatSolutions.TheLemonTree.ui.soon;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.databinding.ActivitySoonBinding;

public class SoonActivity extends BaseActivity {

    ActivitySoonBinding mActivityBinding;
    SoonViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_soon);
        mViewModel = ViewModelProviders.of(this).get(SoonViewModel.class);
        mActivityBinding.setVm(mViewModel);
    }
}
