package com.NatSolutions.TheLemonTree.ui.shop;

/**
 * Created by karim on 5/31/18.
 */

public interface ShopNavigator {
    void openBoutique();
    void openCanLimon();
}
