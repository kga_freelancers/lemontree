package com.NatSolutions.TheLemonTree.ui.reservation;

import com.NatSolutions.TheLemonTree.data.model.Venue;

/**
 * Created by karim on 5/11/18.
 */

public interface ReservationNavigator {
    void openHome();
    void openCheckReservation();
    void openMakeReservation(Venue venue, String date);

}
