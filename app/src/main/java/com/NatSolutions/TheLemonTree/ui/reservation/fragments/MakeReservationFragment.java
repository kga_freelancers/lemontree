package com.NatSolutions.TheLemonTree.ui.reservation.fragments;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseFragment;
import com.NatSolutions.TheLemonTree.custom.Command;
import com.NatSolutions.TheLemonTree.custom.MyDialogFragment;
import com.NatSolutions.TheLemonTree.data.model.Guest;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.Venue;
import com.NatSolutions.TheLemonTree.databinding.FragmentMakeReservationBinding;
import com.NatSolutions.TheLemonTree.ui.guestsList.GuestsListActivity;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationViewModel;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.SnackbarUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;


public class MakeReservationFragment extends BaseFragment {

    FragmentMakeReservationBinding mFragmentBinding;
    ReservationViewModel mViewModel;
    private Venue venue;
    private String date;
    private List<ReservationResponse.Reservation> reservationList = new ArrayList<>();
    private ReservationResponse.Reservation reservation;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentBinding = FragmentMakeReservationBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(ReservationViewModel.class);
        venue = new Gson().fromJson(getArguments().getString(Constants.EXTRA_VENUE), Venue.class);
        date = getArguments().getString(Constants.EXTRA_DATE);
        mFragmentBinding.setVm(mViewModel);
        mFragmentBinding.setView(this);
        mFragmentBinding.setSuccess(false);
        showProgressDialog();
        subscribeEvents();
        return mFragmentBinding.getRoot();
    }

    private void subscribeEvents() {
        mViewModel.getReservationData().observe(getActivity(), new Observer<ReservationResponse>() {
            @Override
            public void onChanged(@Nullable ReservationResponse reservationResponse) {
                hideProgressDialog();
                reservationList = reservationResponse.reservationTypes.reservationList;
            }
        });


        mViewModel.getHouseRulesNextEvent().observe(getActivity(), new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                mViewModel.makeReservation().observe(getActivity(), new Observer<MyResponse>() {
                    @Override
                    public void onChanged(@Nullable MyResponse myResponse) {
                        if (myResponse.status) {
                            mFragmentBinding.setSuccess(true);
                            mFragmentBinding.confirmReservationLayout.rootView.
                                    postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent(getActivity(),
                                                    HomeActivity.class);
                                            intent.putExtra(Constants.EXTRA_OPEN_MY_RESERVATION,
                                                    Constants.EXTRA_OPEN_MY_RESERVATION);
                                            startActivity(intent);
                                        }
                                    }, 6000);
                        }
                    }
                });
            }
        });

    }

    public View.OnClickListener showReservationTypes() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomPopup(reservationList, new Command() {
                    @Override
                    public void execute() {

                    }

                    @Override
                    public void executeWithResult(Object object) {
                        reservation = ((ReservationResponse.Reservation) object);
                        mViewModel.setReservation(reservation);
                        mFragmentBinding.reservationTimeTextView.setText("");
                        mFragmentBinding.reservationTypeTextView.setText(
                                ((ReservationResponse.Reservation) object).reservationType);
                        mViewModel.typeObservableText.set(
                                ((ReservationResponse.Reservation) object).reservationType);

                    }
                });
            }
        };
    }

    public View.OnClickListener showReservationTimes() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reservation == null) {
                    SnackbarUtils.showSnackbar(mFragmentBinding.rootView,
                            getString(R.string.select_type));
                } else {
                    showCustomPopup(reservation.reservationTimeSlotList, new Command() {
                        @Override
                        public void execute() {

                        }

                        @Override
                        public void executeWithResult(Object object) {
                            mFragmentBinding.reservationTimeTextView.setText(
                                    ((ReservationResponse.ReservationTimeSlot) object)
                                            .timeSlotValue);
                            mViewModel.setTimeSlot(((ReservationResponse.ReservationTimeSlot)
                                    object));
                            mViewModel.timeObservableText.set(
                                    ((ReservationResponse.ReservationTimeSlot) object)
                                            .timeSlotValue);
                        }
                    });
                }
            }

        };
    }

    public View.OnClickListener showNumberOfGuestsList() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCustomPopup(getNumberOfGuestsList(), new Command() {
                    @Override
                    public void execute() {

                    }

                    @Override
                    public void executeWithResult(Object object) {
                        mFragmentBinding.numberOfGuestsTextView.setText(object.toString());
                        mViewModel.numberOfGuestsObservableText.set(object.toString());
                    }
                });
            }
        };
    }

    public View.OnClickListener openGuestList() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFragmentBinding.numberOfGuestsTextView
                        .getText().toString().length() == 0) {
                    SnackbarUtils.showSnackbar(mFragmentBinding.rootView,
                            getString(R.string.choose_number_of_gusts));
                } else {
                    Intent intent = new Intent(getActivity(), GuestsListActivity.class);
                    intent.putExtra(Constants.NUMBER_OF_GUESTS, mFragmentBinding
                            .numberOfGuestsTextView
                            .getText().toString());
                    startActivityForResult(intent, 1);
                }
            }
        };
    }

    public static MakeReservationFragment newInstance(Venue venue, String date) {
        MakeReservationFragment fragment = new MakeReservationFragment();
        Bundle args = new Bundle();
        args.putString(Constants.EXTRA_VENUE, new Gson().toJson(venue));
        args.putString(Constants.EXTRA_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    private List<Integer> getNumberOfGuestsList() {
        List<Integer> guestList = new ArrayList<>();
        for (int i = 2; i <= 60; i++) {
            guestList.add(i);
        }
        return guestList;
    }

    public View.OnClickListener confirmListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                MyDialogFragment dialogFragment = new MyDialogFragment();
                dialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                dialogFragment.houseRulesList = mViewModel.getVenueHouseRulesList();
                dialogFragment.setCancelable(false);
                dialogFragment.show(fm, "info_popup");
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            List<Guest> guests = new Gson()
                    .fromJson(data.getStringExtra("guestList"),
                            new TypeToken<List<Guest>>() {
                            }.getType());
            setGuestsNames(guests);
        }
    }

    private void setGuestsNames(List<Guest> guestList) {
        List<Guest> guests = new ArrayList<>();
        String names = "";
        for (Guest guest : guestList) {
            if (guest.guestName != null) {
                names += guest.guestName + "\n";
                guests.add(guest);
            }
        }
        mFragmentBinding.namesTextView.setText(names);
        mViewModel.setGuestList(guests);
        mViewModel.guestsObservableText.set(names);

    }
}
