package com.NatSolutions.TheLemonTree.ui.home.fragments.myMusic;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.base.BaseFragment;
import com.NatSolutions.TheLemonTree.data.model.Track;
import com.NatSolutions.TheLemonTree.data.model.UserTracksResponse;
import com.NatSolutions.TheLemonTree.data.model.source.MusicRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.databinding.FragmentMyMusicBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 26/05/18.
 */

public class MyMusicFragment extends BaseFragment {

    FragmentMyMusicBinding binding;
    MyMusicAdapter mAdapter;
    List<Track> mFavoriteTracks = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentMyMusicBinding.inflate(inflater, container,
                false);
        initializeRecyclerView();
        getUserFavoriteTracks();
        return binding.getRoot();
    }

    private void initializeRecyclerView() {
        mAdapter = new MyMusicAdapter(getActivity());
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(mAdapter);
    }

    public void getUserFavoriteTracks() {
        binding.progressBar.setVisibility(View.VISIBLE);
        MusicRepository musicRepository = new MusicRepository();

//        musicRepository.getUserFavoriteTracks(UserDataSource.getUser().userID).
//                observe(this, new Observer<FavoriteTrackResponse>() {
//                    @Override
//                    public void onChanged(@Nullable FavoriteTrackResponse favoriteTrackResponse) {
//                        binding.progressBar.setVisibility(View.GONE);
//                        if (favoriteTrackResponse.favoriteTracks.size() > 0) {
//                            binding.recyclerView.setVisibility(View.VISIBLE);
//                            binding.noDataTextView.setVisibility(View.GONE);
//                            mAdapter.setData(favoriteTrackResponse.favoriteTracks);
//                        } else {
//                            binding.recyclerView.setVisibility(View.GONE);
//                            binding.noDataTextView.setVisibility(View.VISIBLE);
//                        }
//                    }
//                });


        musicRepository.getUserTracks(UserDataSource.getUser().userID).
                observe(this, new Observer<UserTracksResponse>() {
                    @Override
                    public void onChanged(@Nullable UserTracksResponse userTracksResponse) {
                        binding.progressBar.setVisibility(View.GONE);
                        if (userTracksResponse != null) {
                            getFavoriteTracks(userTracksResponse.tracks);
                            if (mFavoriteTracks.size() > 0) {
                                binding.recyclerView.setVisibility(View.VISIBLE);
                                binding.noDataTextView.setVisibility(View.GONE);
                                mAdapter.setData(mFavoriteTracks);
                            } else {
                                binding.recyclerView.setVisibility(View.GONE);
                                binding.noDataTextView.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
    }

    private void getFavoriteTracks(List<Track> mTracks) {
        if (mTracks != null && mTracks.size() > 0) {
            for (int i = 0; i < mTracks.size(); i++) {
                if (mTracks.get(i).isFavourite) mFavoriteTracks.add(mTracks.get(i));
            }
        }
    }

}
