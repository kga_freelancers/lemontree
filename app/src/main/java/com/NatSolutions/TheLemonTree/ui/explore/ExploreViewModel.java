package com.NatSolutions.TheLemonTree.ui.explore;

import android.app.Application;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;

/**
 * Created by root on 11/05/18.
 */

public class ExploreViewModel extends BaseViewModel {

    public ExploreViewModel(@NonNull Application application) {
        super(application);
    }
}
