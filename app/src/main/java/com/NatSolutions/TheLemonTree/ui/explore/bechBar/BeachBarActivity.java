package com.NatSolutions.TheLemonTree.ui.explore.bechBar;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityBeachBarBinding;

import java.util.ArrayList;
import java.util.List;

public class BeachBarActivity extends BaseActivity {

    ActivityBeachBarBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_beach_bar);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getBechBarItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getBechBarItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.beach_bar_1,
                getString(R.string.beach_bar_text_4)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.beach_bar_2,
                getString(R.string.beach_bar_text_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.beach_bar_3_new,
                getString(R.string.beach_bar_text_3)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.beach_bar_4,
                getString(R.string.beach_bar_text_3)));

        return viewPagerItemModelList;
    }
}
