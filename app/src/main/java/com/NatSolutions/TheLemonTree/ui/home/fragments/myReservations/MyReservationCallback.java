package com.NatSolutions.TheLemonTree.ui.home.fragments.myReservations;

import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;

/**
 * Created by root on 26/05/18.
 */

public interface MyReservationCallback {

    void openReservationDetails(MyReservationsResponse.Reservation reservation);
}
