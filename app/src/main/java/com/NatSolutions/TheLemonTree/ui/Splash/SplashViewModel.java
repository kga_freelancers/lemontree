package com.NatSolutions.TheLemonTree.ui.Splash;

import android.app.Application;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;

/**
 * Created by root on 06/05/18.
 */

public class SplashViewModel extends BaseViewModel {

    public SplashViewModel(@NonNull Application application) {
        super(application);
    }
}
