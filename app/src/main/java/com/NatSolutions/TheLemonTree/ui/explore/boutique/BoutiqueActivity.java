package com.NatSolutions.TheLemonTree.ui.explore.boutique;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityBoutiqueBinding;

import java.util.ArrayList;
import java.util.List;

public class BoutiqueActivity extends BaseActivity {

    ActivityBoutiqueBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_boutique);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(), getMarassiItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getMarassiItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.boutique_1,
                getString(R.string.boutique_text_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.boutique_2,
                getString(R.string.boutique_text_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.boutique_3,
                getString(R.string.boutique_text_3)));
        return viewPagerItemModelList;
    }

}
