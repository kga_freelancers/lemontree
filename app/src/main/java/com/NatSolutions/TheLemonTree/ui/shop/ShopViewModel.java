package com.NatSolutions.TheLemonTree.ui.shop;

import android.app.Application;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;

/**
 * Created by karim on 5/17/18.
 */

public class ShopViewModel extends BaseViewModel {
    public ShopViewModel(@NonNull Application application) {
        super(application);
    }
}
