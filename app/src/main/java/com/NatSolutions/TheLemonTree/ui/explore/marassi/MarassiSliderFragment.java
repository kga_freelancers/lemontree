package com.NatSolutions.TheLemonTree.ui.explore.marassi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.MarassiSliderFragmentBinding;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationActivity;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;


/**
 * Created by amr on 21/06/17.
 */

public class MarassiSliderFragment extends Fragment {

    MarassiSliderFragmentBinding mFragmentBinding;
    ViewPagerItemModel itemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentBinding = MarassiSliderFragmentBinding.inflate(inflater, container, false);

        itemModel = new Gson()
                .fromJson(getArguments()
                                .getString("itemModel")
                        , ViewPagerItemModel.class);
        int position = getArguments().getInt("position");

        mFragmentBinding.setView(this);
        mFragmentBinding.sliderImageView.setImageResource(itemModel.imageRes);
        setText(position);
        return mFragmentBinding.getRoot();
    }

    private void setText(int position) {
        switch (position) {
            case 0:
                mFragmentBinding.bottomLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.bottomTextView.setText(itemModel.message);
                break;
            case 1:
                mFragmentBinding.centerLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.centerTextView.setText(itemModel.message);
                break;
            case 2:
                mFragmentBinding.topLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.topTextView.setText(itemModel.message);
                mFragmentBinding.locationButton.setVisibility(View.VISIBLE);
                mFragmentBinding.reserveButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    public View.OnClickListener openLocation() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=loc:" +
                                Constants.MARASSI_MAP_LOCATION));
                startActivity(intent);
            }
        };
    }

    public View.OnClickListener openReservation() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ReservationActivity.class));
            }
        };
    }

    public static MarassiSliderFragment newInstance(ViewPagerItemModel model, int itemPosition) {
        MarassiSliderFragment homeGallerySliderFragment = new MarassiSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("itemModel", new Gson().toJson(model));
        bundle.putInt("position", itemPosition);
        homeGallerySliderFragment.setArguments(bundle);
        return homeGallerySliderFragment;
    }


}