package com.NatSolutions.TheLemonTree.ui.explore.bechBar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by root on 12/03/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private int pagesCount;
    private List<ViewPagerItemModel> galleryList = new ArrayList<>();


    public ViewPagerAdapter(FragmentManager fm, List<ViewPagerItemModel> list) {
        super(fm);
        this.pagesCount = list.size();
        this.galleryList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return BeachBarSliderFragment.newInstance(galleryList.get(position),position);
    }

    @Override
    public int getCount() {
        return pagesCount;
    }


}
