package com.NatSolutions.TheLemonTree.ui.explore.familyAndFriends;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.ActivityFamilyAndFriendsBinding;

import java.util.ArrayList;
import java.util.List;

public class FamilyAndFriendsActivity extends BaseActivity {

    ActivityFamilyAndFriendsBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_family_and_friends);
        initGalleryViewPager();
    }

    private void initGalleryViewPager() {
        ViewPagerAdapter adapter =
                new ViewPagerAdapter(getSupportFragmentManager(),
                        getFamilyAndFriendsBarItems());
        mActivityBinding.viewPager.setAdapter(adapter);
    }

    private List<ViewPagerItemModel> getFamilyAndFriendsBarItems() {
        List<ViewPagerItemModel> viewPagerItemModelList = new ArrayList<>();
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.family_and_friends_1,
                getString(R.string.family_and_friends_text_1)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.family_and_friends_2_new,
                getString(R.string.family_and_friends_text_2)));
        viewPagerItemModelList.add(new ViewPagerItemModel(R.drawable.family_and_friends_3_new,
                getString(R.string.family_and_friends_text_3)));

        return viewPagerItemModelList;
    }
}
