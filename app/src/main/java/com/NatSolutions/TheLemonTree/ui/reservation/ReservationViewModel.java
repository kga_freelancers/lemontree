package com.NatSolutions.TheLemonTree.ui.reservation;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.Guest;
import com.NatSolutions.TheLemonTree.data.model.CheckReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.GuestListRequest;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.ReservationResponse;
import com.NatSolutions.TheLemonTree.data.model.Venue;
import com.NatSolutions.TheLemonTree.data.model.VenueResponse;
import com.NatSolutions.TheLemonTree.data.model.source.ReservationRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karim on 5/11/18.
 */

public class ReservationViewModel extends BaseViewModel {

    private ReservationRepository reservationRepository;
    public ObservableField<String> locationObservable = new ObservableField<>("");
    public ObservableField<String> notes = new ObservableField<>("");
    public ObservableField<Boolean> checkLocationObservable = new ObservableField<>(false);
    public ObservableField<String> dateObservable = new ObservableField<>("");
    public ObservableField<Boolean> checkDateObservable = new ObservableField<>(false);
    public ObservableField<Boolean> isValid = new ObservableField<>(false);

    private SingleLiveEvent<Void> openMakeReservation = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> houseRulesNextEvent = new SingleLiveEvent<>();
    public SingleLiveEvent<Venue> venueEvent = new SingleLiveEvent<>();
    public SingleLiveEvent<String> dateEvent = new SingleLiveEvent<>();

    public ObservableField<Boolean> typeObservable = new ObservableField<>(false);
    public ObservableField<Boolean> timeObservable = new ObservableField<>(false);
    public ObservableField<Boolean> numberOfGuestsObservable = new ObservableField<>(false);
    public ObservableField<Boolean> guestsObservable = new ObservableField<>(false);
    public ObservableField<Boolean> isGuestList = new ObservableField<>(false);

    public ObservableField<String> typeObservableText = new ObservableField<>("");
    public ObservableField<String> timeObservableText = new ObservableField<>("");
    public ObservableField<String> numberOfGuestsObservableText = new ObservableField<>("");
    public ObservableField<String> guestsObservableText = new ObservableField<>("");

    private List<Guest> guestList = new ArrayList<>();
    private List<String> houseRulesList = new ArrayList<>();
    private Venue venue;
    private ReservationResponse.Reservation reservation;
    private ReservationResponse.ReservationTimeSlot reservationTimeSlot;

    public ReservationViewModel(@NonNull Application application) {
        super(application);
        reservationRepository = new ReservationRepository(this.getApplication());
        validateCheckReservation();
        validateMakeReservationFields();
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
        venueEvent.setValue(venue);
    }

    public void setVenueHouseRules(List<String> houseRulesList) {
        this.houseRulesList = houseRulesList;
    }

    public List<String> getVenueHouseRulesList() {
        return this.houseRulesList;
    }

    public LiveData<VenueResponse> getAllVenues() {
        return reservationRepository.getAllVenues();
    }

    public LiveData<CheckReservationResponse> checkReservationDate() {
        return reservationRepository.checkReservationDate
                (dateEvent.getValue(), venue.venueID);
    }

    public LiveData<ReservationResponse> getReservationData() {
        return reservationRepository.getReservationData(venue.venueID);
    }

    public void openMakeReservation() {
        openMakeReservation.call();
    }

    public void houseRulesNextClick() {
        houseRulesNextEvent.call();
    }

    public SingleLiveEvent<Void> getHouseRulesNextEvent() {
        return houseRulesNextEvent;
    }

    private void validateCheckReservation() {
        locationObservable.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (locationObservable.get().length() > 0 && venue.isActive) {
                    checkLocationObservable.set(true);
                } else {
                    checkLocationObservable.set(false);
                }
            }
        });
        dateObservable.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (dateObservable.get().length() > 0) {
                    checkDateObservable.set(true);
                } else {
                    checkDateObservable.set(false);
                }
            }
        });
    }

    public SingleLiveEvent getMakeReservationEvent() {
        return openMakeReservation;
    }

    public void setGuestList(List<Guest> guestList) {
        this.guestList = guestList;
    }

    public void setReservation(ReservationResponse.Reservation reservation) {
        this.reservation = reservation;
    }

    public void setTimeSlot(ReservationResponse.ReservationTimeSlot reservationTimeSlot) {
        this.reservationTimeSlot = reservationTimeSlot;
    }


    public void validateMakeReservationFields() {
        typeObservableText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (typeObservableText.get().length() > 0) {
                    typeObservable.set(true);
                    validateMakeReservation();
                }
            }
        });
        timeObservableText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (timeObservableText.get().length() > 0) {
                    timeObservable.set(true);
                    validateMakeReservation();
                }
            }
        });
        numberOfGuestsObservableText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (numberOfGuestsObservableText.get().length() > 0) {
                    numberOfGuestsObservable.set(true);
                    validateMakeReservation();
                }
            }
        });
        guestsObservableText.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (guestsObservableText.get().length() > 0) {
                    guestsObservable.set(true);
                    validateMakeReservation();
                }
            }
        });
    }


    public void validateMakeReservation() {
        if (isGuestList.get()) {
            if (typeObservable.get() && timeObservable.get() && numberOfGuestsObservable.get()
                    && guestsObservable.get()) {
                isValid.set(true);
            }
        } else {
            if (typeObservable.get() && timeObservable.get() && numberOfGuestsObservable.get()) {
                isValid.set(true);
            }
        }
    }

    public LiveData<MyResponse> makeReservation() {
        GuestListRequest guestListRequest = new GuestListRequest();
        guestListRequest.guestList = guestList;
        return reservationRepository.makeReservation(Constants.CONTENT_TYPE,
                Constants.ACCEPT,
                UserDataSource.getUser().userID,
                Integer.parseInt(venue.venueID),
                reservation.reservationTypeID,
                reservationTimeSlot.timeSlotID,
                dateEvent.getValue(),
                Integer.parseInt(numberOfGuestsObservableText.get()),
                notes.get(),
                guestListRequest);
    }

}
