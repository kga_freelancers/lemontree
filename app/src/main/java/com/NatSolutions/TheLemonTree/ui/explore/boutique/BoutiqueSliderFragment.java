package com.NatSolutions.TheLemonTree.ui.explore.boutique;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.data.model.ViewPagerItemModel;
import com.NatSolutions.TheLemonTree.databinding.BoutiqueSliderFragmentBinding;
import com.google.gson.Gson;


/**
 * Created by amr on 21/06/17.
 */

public class BoutiqueSliderFragment extends Fragment {

    BoutiqueSliderFragmentBinding mFragmentBinding;
    ViewPagerItemModel itemModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentBinding = BoutiqueSliderFragmentBinding.inflate(inflater, container, false);

        itemModel = new Gson()
                .fromJson(getArguments()
                                .getString("itemModel")
                        , ViewPagerItemModel.class);
        int position = getArguments().getInt("position");

        mFragmentBinding.sliderImageView.setImageResource(itemModel.imageRes);
        setText(position);
        return mFragmentBinding.getRoot();
    }

    private void setText(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                mFragmentBinding.bottomLayer.setVisibility(View.VISIBLE);
                mFragmentBinding.bottomTextView.setText(itemModel.message);
                break;
        }
    }

    public static BoutiqueSliderFragment newInstance(ViewPagerItemModel model, int itemPosition) {
        BoutiqueSliderFragment homeGallerySliderFragment = new BoutiqueSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("itemModel", new Gson().toJson(model));
        bundle.putInt("position", itemPosition);
        homeGallerySliderFragment.setArguments(bundle);
        return homeGallerySliderFragment;
    }


}