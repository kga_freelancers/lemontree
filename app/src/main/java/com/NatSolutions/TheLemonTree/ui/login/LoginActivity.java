package com.NatSolutions.TheLemonTree.ui.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.data.model.UserResponse;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.databinding.ActivityLoginBinding;
import com.NatSolutions.TheLemonTree.ui.completeProfile.CompleteProfileActivity;
import com.NatSolutions.TheLemonTree.ui.home.HomeActivity;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.NatSolutions.TheLemonTree.utils.SnackbarUtils;
import com.NatSolutions.TheLemonTree.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_COMPELETE_PROFILE;

public class LoginActivity extends BaseActivity implements LoginNavigator {

    ActivityLoginBinding mActivityBinding;
    LoginViewModel mViewModel;

    private final List<String> facebookPermissions = Arrays.asList("email", "public_profile"
            , "user_birthday");
    private CallbackManager callbackManager;
    CompositeDisposable mCompositeDisposable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        bindActivity();
        subscribeToNavigationChanges();
        checkUser();
        Utils.getHashKey(this);
        mCompositeDisposable = new CompositeDisposable();
    }


    private void checkUser() {
        if (UserDataSource.getUser() != null) {
//            if (!UserDataSource.getFacebookID().contentEquals("")) {
            openHome();
//            }
        }
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mActivityBinding.setVm(mViewModel);
    }

    private void subscribeToNavigationChanges() {
        mViewModel.getFacebookClickEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                setupFacebook();
            }
        });
    }

    private void setupFacebook() {
        if (!Utils.isConnectingToInternet()) {
            SnackbarUtils.showSnackbar(mActivityBinding.rootView,
                    getString(R.string.internet_is_not_connected));
            return;
        } else if (DataUtil.getData(this, Constants.FCM_TOKEN, "").
                contentEquals("") || DataUtil.getData(this,
                Constants.FCM_TOKEN,"") == null) {
            SnackbarUtils.showSnackbar(mActivityBinding.rootView,
                    "Please wait a moment");
        } else {
            LoginManager.getInstance().logInWithReadPermissions(this, facebookPermissions);
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            loadFacebookUser(loginResult.getAccessToken());
                        }

                        @Override
                        public void onCancel() {
                            Log.d("dsd", "sdsd");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            SnackbarUtils.showSnackbar(mActivityBinding.rootView,
                                    exception.getLocalizedMessage());
                        }
                    });
        }
    }

    private void loadFacebookUser(AccessToken accessToken) {

        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (response.getError() != null)
                            SnackbarUtils.showSnackbar(mActivityBinding.rootView,
                                    response.getError().getException().toString());
                        else
                            loginWithFacebook(response.getJSONObject());
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,picture.type(large),email,birthday,gender,hometown,link,location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginWithFacebook(final JSONObject graphObject) {
        DataUtil.saveData(this, Constants.FB_ID,
                graphObject.optString("id", ""));

        mViewModel.loginWithFacebook().observe(this, new Observer<UserResponse>() {
            @Override
            public void onChanged(@Nullable UserResponse userResponse) {
                if (userResponse.status) {
                    UserDataSource.saveUser(userResponse.user);
                    openHome();
                } else {
                    openCompeleteProfile(graphObject);
                }
                finish();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void openHome() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    private void openCompeleteProfile(JSONObject graphObject) {
        Intent intent = new Intent(this, CompleteProfileActivity.class);
        intent.putExtra(EXTRA_COMPELETE_PROFILE, new Gson().toJson(graphObject));
        startActivity(intent);
    }
}
