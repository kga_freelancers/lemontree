package com.NatSolutions.TheLemonTree.ui.editProfile;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.EditUserResponse;
import com.NatSolutions.TheLemonTree.data.model.User;
import com.NatSolutions.TheLemonTree.data.model.source.UserRepository;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;

/**
 * Created by root on 06/05/18.
 */

public class EditProfileViewModel extends BaseViewModel {

    public ObservableField<String> userNameObservable =
            new ObservableField<>(UserDataSource.getUser().userName);
    public ObservableField<String> birthDateObservable =
            new ObservableField<>(UserDataSource.getUser().userBirthdate);
    public ObservableField<String> genderObservable =
            new ObservableField<>(UserDataSource.getUser().returnGender());
    public ObservableField<String> emailObservable =
            new ObservableField<>(UserDataSource.getUser().userEmail);
    public ObservableField<String> mobileObservable =
            new ObservableField<>(UserDataSource.getUser().userPhone);
    public ObservableField<String> addressObservable =
            new ObservableField<>(UserDataSource.getUser().userAddress);

    public ObservableField<Boolean> userNameCheckObservable = new ObservableField<>(false);
    public ObservableField<Boolean> emailCheckObservable = new ObservableField<>(true);
    public ObservableField<Boolean> mobileCheckObservable = new ObservableField<>(true);

    private UserRepository userRepository;


    public EditProfileViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(this.getApplication());
        validateCheckReservation();
    }

    public LiveData<EditUserResponse> updateUser() {
        return userRepository.editProfile(editUser());
    }


    private void validateCheckReservation() {
        emailObservable.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (emailObservable.get().length() > 0) {
                    emailCheckObservable.set(true);
                } else {
                    emailCheckObservable.set(false);
                }
            }
        });
        mobileObservable.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (mobileObservable.get().length() > 0) {
                    mobileCheckObservable.set(true);
                } else {
                    mobileCheckObservable.set(false);
                }
            }
        });
    }


    private User editUser() {
        User user = UserDataSource.getUser();
        user.userGender = ("" + genderObservable.get().charAt(0)).toLowerCase();
        user.userName = userNameObservable.get();
        user.userBirthdate = birthDateObservable.get();
        user.userEmail = emailObservable.get();
        user.userPhone = mobileObservable.get();
//        String birthDate = birthDateObservable.get();
//        String[] parts = birthDate.split("/");
//        user.userBirthdate = parts[2] + "-" + parts[0] + "-" + parts[1];
        user.userAddress = addressObservable.get();
        return user;
    }

}
