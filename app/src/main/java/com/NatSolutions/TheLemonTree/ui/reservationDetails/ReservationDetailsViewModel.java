package com.NatSolutions.TheLemonTree.ui.reservationDetails;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.NatSolutions.TheLemonTree.base.BaseViewModel;
import com.NatSolutions.TheLemonTree.data.model.ReservationInfoResponse;
import com.NatSolutions.TheLemonTree.data.model.source.ReservationRepository;

/**
 * Created by karim on 6/2/18.
 */

public class ReservationDetailsViewModel extends BaseViewModel {

    ReservationRepository reservationRepository;

    public ReservationDetailsViewModel(@NonNull Application application) {
        super(application);
        reservationRepository = new ReservationRepository(this.getApplication());
    }

    public LiveData<ReservationInfoResponse> getReservationInfoData(int reservationID) {
        return reservationRepository.getReservationInfoData(reservationID);
    }

}
