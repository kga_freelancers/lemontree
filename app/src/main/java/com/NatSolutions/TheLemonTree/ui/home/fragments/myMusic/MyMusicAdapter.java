package com.NatSolutions.TheLemonTree.ui.home.fragments.myMusic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.MyResponse;
import com.NatSolutions.TheLemonTree.data.model.Track;
import com.NatSolutions.TheLemonTree.data.model.source.local.UserDataSource;
import com.NatSolutions.TheLemonTree.data.model.source.remote.LemonTreeWebService;
import com.NatSolutions.TheLemonTree.ui.musicPlayer.MusicPlayerActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_TRACK;
import static com.NatSolutions.TheLemonTree.utils.Constants.EXTRA_TRACK_LIST;


/**
 * Created by amr on 26/08/17.
 */

public class MyMusicAdapter extends RecyclerView.Adapter<MyMusicAdapter.MyMusicViewHolder> {


    private List<Track> items = new ArrayList<>();

    private Context mContext;
    private LayoutInflater inflater;
    private String mTrackList;


    public MyMusicAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyMusicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_my_music, parent, false);
        return new MyMusicViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final MyMusicViewHolder holder, final int position) {
        holder.progressBar.setVisibility(View.GONE);
        holder.removeFavorite.setVisibility(View.VISIBLE);

        final Track track = items.get(position);
        holder.title.setText(track.trackName);
        holder.musicName.setText(track.trackArtist);
        holder.time.setText(track.trckDescription);

        Glide.with(mContext).load(track.trackIMage)
                .apply(new RequestOptions().placeholder(R.drawable.sound_bg))
                .into(holder.trackImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MusicPlayerActivity.class);
                intent.putExtra(EXTRA_TRACK, new Gson().toJson(track));
                intent.putExtra(EXTRA_TRACK_LIST, mTrackList);
                mContext.startActivity(intent);
            }
        });

        holder.removeFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.removeFavorite.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);

                LemonTreeWebService lemonTreeWebService = LemonTreeWebService.retrofit.
                        create(LemonTreeWebService.class);
                Call<MyResponse> call = lemonTreeWebService.removeFavoriteTrack(items.get(position).trackId,
                        UserDataSource.getUser().userID);
                call.enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(Call<MyResponse> call,
                                           Response<MyResponse> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.removeFavorite.setVisibility(View.VISIBLE);
                            if (response.isSuccessful()) {
                                showPopUp("Track removed from favorite list",position);
                            } else {
                                Toast.makeText(mContext,"some thing went wrong",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<MyResponse> call, Throwable t) {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.removeFavorite.setVisibility(View.VISIBLE);
                        Toast.makeText(mContext,"some thing went wrong",Toast.LENGTH_LONG).show();                    }
                });
            }
        });
    }


    public void showPopUp(String message , final int position) {
        try {
            new AlertDialog.Builder(mContext)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            items.remove(position);
                            notifyItemRemoved(position);
                            notifyDataSetChanged();
                        }
                    })
                    .show();
        } catch (Exception e) {
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Track> items) {
        this.items = items;
        notifyDataSetChanged();
        mTrackList = new Gson().toJson(items, new TypeToken<List<Track>>() {
        }.getType());
    }

    public class MyMusicViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView musicName;
        private TextView time;
        private ImageView trackImage;
        private ImageView removeFavorite;
        private ProgressBar progressBar;

        public MyMusicViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_textView);
            musicName = (TextView) itemView.findViewById(R.id.music_name);
            time = (TextView) itemView.findViewById(R.id.time_textView);
            trackImage = (ImageView) itemView.findViewById(R.id.thum_imageView);
            removeFavorite = (ImageView) itemView.findViewById(R.id.remove_favorite_track);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }
}
