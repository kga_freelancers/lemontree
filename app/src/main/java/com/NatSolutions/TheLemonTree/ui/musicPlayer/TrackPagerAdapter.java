package com.NatSolutions.TheLemonTree.ui.musicPlayer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.NatSolutions.TheLemonTree.data.model.Track;

import java.util.List;

/**
 * Created by karim on 11/10/17.
 */

public class TrackPagerAdapter extends FragmentPagerAdapter {

    private int pagesCount;
    private List<Track> mDataSet;

    public TrackPagerAdapter(FragmentManager fm, List<Track> mDataSet) {
        super(fm);
        this.mDataSet = mDataSet;
        this.pagesCount = mDataSet.size();
    }

    @Override
    public Fragment getItem(int position) {
        return TrackFragment.newInstance(mDataSet.get(position));
    }

    @Override
    public int getCount() {
        return pagesCount;
    }


}