package com.NatSolutions.TheLemonTree.ui.home;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.SideMenuModel;
import com.NatSolutions.TheLemonTree.databinding.IndexSideMenuBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karim on 5/26/18.
 */

public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.SideMenuViewHolder> {

    private List<SideMenuModel> sideMenuItems;
    private Context mContext;
    private SideMenuNavigator sideMenuNavigator;

    public SideMenuAdapter(Context context, SideMenuNavigator sideMenuNavigator) {
        this.mContext = context;
        sideMenuItems = new ArrayList<>();
        sideMenuItems.add(new SideMenuModel(context.getString(R.string.my_notifications),
                ContextCompat.getDrawable(context, R.drawable.ic_my_notifications), true));
        sideMenuItems.add(new SideMenuModel(context.getString(R.string.my_reservations),
                ContextCompat.getDrawable(context, R.drawable.ic_my_reservations), false));
        sideMenuItems.add(new SideMenuModel(context.getString(R.string.music_favorites),
                ContextCompat.getDrawable(context, R.drawable.ic_my_music), false));
        sideMenuItems.add(new SideMenuModel(context.getString(R.string.about_us),
                ContextCompat.getDrawable(context, R.drawable.ic_about), false));
        this.sideMenuNavigator = sideMenuNavigator;

    }


    @Override
    public SideMenuAdapter.SideMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        IndexSideMenuBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.index_side_menu, parent, false);

        return new SideMenuAdapter.SideMenuViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(SideMenuAdapter.SideMenuViewHolder holder, final int position) {
        holder.bind(sideMenuItems.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideMenuNavigator.sideMenuItemsNavigator(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sideMenuItems.size();
    }


    public class SideMenuViewHolder extends RecyclerView.ViewHolder {

        private IndexSideMenuBinding indexSideMenuBinding;

        public SideMenuViewHolder(IndexSideMenuBinding binding) {
            super(binding.getRoot());
            this.indexSideMenuBinding = binding;
        }

        public void bind(SideMenuModel sideMenuItem) {
            indexSideMenuBinding.setItem(sideMenuItem);
            indexSideMenuBinding.executePendingBindings();
        }

    }
}