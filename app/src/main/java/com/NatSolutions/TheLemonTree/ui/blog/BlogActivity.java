package com.NatSolutions.TheLemonTree.ui.blog;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.BaseActivity;
import com.NatSolutions.TheLemonTree.databinding.ActivityBlogBinding;

public class BlogActivity extends BaseActivity {

    ActivityBlogBinding mActivityBinding;
    BlogViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindActivity();
    }

    private void bindActivity() {
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_blog);
        mViewModel = ViewModelProviders.of(this).get(BlogViewModel.class);
        mActivityBinding.setVm(mViewModel);
    }
}
