package com.NatSolutions.TheLemonTree.ui.guestsList;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.Guest;
import com.NatSolutions.TheLemonTree.databinding.ActivityGuestsBinding;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationActivity;
import com.NatSolutions.TheLemonTree.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class
GuestsListActivity extends AppCompatActivity {

    ActivityGuestsBinding mActivityBinding;
    GuestsAdapter adapter;
    int numberOfGuests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_guests);

        numberOfGuests = Integer.parseInt(getIntent()
                .getStringExtra(Constants.NUMBER_OF_GUESTS));
        initRecyclerView();

        mActivityBinding.confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getGuestsNames();
            }
        });

    }

    private void initRecyclerView() {
        adapter = new GuestsAdapter(this);
        mActivityBinding.recyclerView.setHasFixedSize(true);
        mActivityBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mActivityBinding.recyclerView.setAdapter(adapter);
        adapter.setData(getGuests());
    }

    private List<Guest> getGuests() {
        List<Guest> guests = new ArrayList<>();
        for (int i = 1; i <= numberOfGuests; i++) {
            guests.add(new Guest());
        }
        return guests;
    }

    private void getGuestsNames() {
        Intent intent = new Intent(this, ReservationActivity.class);
        intent.putExtra("guestList", new Gson()
                .toJson(adapter.guestList, new TypeToken<List<Guest>>() {
                }.getType()));
        setResult(RESULT_OK, intent);
        finish();
    }


}