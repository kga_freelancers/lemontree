package com.NatSolutions.TheLemonTree.ui.home.fragments.myReservations;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.data.model.MyReservationsResponse;
import com.NatSolutions.TheLemonTree.databinding.ItemMyReservationBinding;
import com.NatSolutions.TheLemonTree.utils.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/03/18.
 */

public class MyReservationsAdapter extends RecyclerView.Adapter<MyReservationsAdapter.MyReservationViewHolder> {

    Context mContext;
    List<MyReservationsResponse.Reservation> reservationList = new ArrayList<>();
    MyReservationCallback myReservationCallback;

    public MyReservationsAdapter(Context context, MyReservationCallback myReservationCallback) {
        this.mContext = context;
        this.myReservationCallback = myReservationCallback;
    }

    @NonNull
    @Override
    public MyReservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMyReservationBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_my_reservation, parent, false);

        return new MyReservationViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyReservationViewHolder holder, int position) {
        final MyReservationsResponse.Reservation reservation = reservationList.get(position);
        holder.bind(new MyReservationItemViewModel(reservation));
        if (position % 2 == 0) {
            holder.binding.bgImageView.setBackgroundResource(R.drawable.bg_left_reservation);
        } else {
            holder.binding.bgImageView.setBackgroundResource(R.drawable.bg_right_reservations);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myReservationCallback.openReservationDetails(reservation);
            }
        });


//        if (reservationList.get(position).cancelled == null
//                && reservationList.get(position).depositAmount != null
//                && reservationList.get(position).depositPayed != null
//                && reservationList.get(position).depositPayed.equalsIgnoreCase("true")) {
//            holder.binding.statusTextView.setText("Confirmed & Paid");
//            holder.binding.statusTextView.setTextColor(App.getContext().getResources().getColor(R.color.Green));
//
//        }
        if (reservationList.get(position).confirmed != null
                && reservationList.get(position).confirmed.equals("false")
                && reservationList.get(position).cancelled == null) {
            holder.binding.statusTextView.setText(R.string.disconfirmed);
            holder.binding.statusTextView.setTextColor(App.getContext()
                    .getResources().getColor(R.color.new_red));
            holder.binding.disConfirmedReasonLayout.setVisibility(View.VISIBLE);

        } else if (reservationList.get(position).cancelled == null
                && reservationList.get(position).depositAmount != null
                && reservationList.get(position).depositPayed != null
                && reservationList.get(position).depositPayed
                .equalsIgnoreCase("false")) {
            holder.binding.statusTextView.setText(R.string.waiting_deposit);
            holder.binding.statusTextView.setTextColor
                    (App.getContext().getResources().getColor(R.color.Blue));
            holder.binding.disConfirmedReasonLayout.setVisibility(View.GONE);

        } else if (reservationList.get(position).cancelled != null
                && reservationList.get(position).cancelled.equalsIgnoreCase("true")) {
            holder.binding.statusTextView.setText(R.string.canceled);
            holder.binding.statusTextView.setTextColor
                    (App.getContext().getResources().getColor(R.color.new_red));
            holder.binding.disConfirmedReasonLayout.setVisibility(View.GONE);

        } else if (reservationList.get(position).cancelled == null &&
                reservationList.get(position).confirmed == null) {
            holder.binding.statusTextView.setText(R.string.pending);
            holder.binding.statusTextView.setTextColor
                    (App.getContext().getResources().getColor(R.color.yellow));
            holder.binding.disConfirmedReasonLayout.setVisibility(View.GONE);

        } else if (reservationList.get(position).confirmed != null
                && reservationList.get(position).confirmed.equals("true")
                && reservationList.get(position).cancelled == null) {
            holder.binding.statusTextView.setText(R.string.confirmed);
            holder.binding.statusTextView.setTextColor
                    (App.getContext().getResources().getColor(R.color.Green));
            holder.binding.disConfirmedReasonLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return reservationList.size();
    }

    public class MyReservationViewHolder extends RecyclerView.ViewHolder {

        private ItemMyReservationBinding binding;

        public MyReservationViewHolder(ItemMyReservationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MyReservationItemViewModel itemViewModle) {
            binding.setVm(itemViewModle);
            binding.executePendingBindings();
        }
    }

    public void setData(List<MyReservationsResponse.Reservation> reservationList) {
        this.reservationList = reservationList;
        notifyDataSetChanged();
    }

}
