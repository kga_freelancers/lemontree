package com.NatSolutions.TheLemonTree.ui.home.fragments.home;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NatSolutions.TheLemonTree.base.BaseFragment;
import com.NatSolutions.TheLemonTree.databinding.FragmentHomeBinding;
import com.NatSolutions.TheLemonTree.ui.blog.BlogActivity;
import com.NatSolutions.TheLemonTree.ui.explore.ExploreActivity;
import com.NatSolutions.TheLemonTree.ui.home.HomeNavigator;
import com.NatSolutions.TheLemonTree.ui.home.HomeViewModel;
import com.NatSolutions.TheLemonTree.ui.listen.ListenActivity;
import com.NatSolutions.TheLemonTree.ui.reservation.ReservationActivity;
import com.NatSolutions.TheLemonTree.ui.shop.ShopActivity;


public class HomeFragment extends BaseFragment implements HomeNavigator {

    FragmentHomeBinding mFragmentBinding;
    HomeViewModel mViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentBinding = FragmentHomeBinding.inflate(inflater, container,
                false);
        mViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        mFragmentBinding.setVm(mViewModel);
        mFragmentBinding.setView(this);
        subscribeToNavigationChanges();
        return mFragmentBinding.getRoot();
    }

    private void subscribeToNavigationChanges() {
        mViewModel.getExploreEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void s) {
                openExplore();
            }
        });

        mViewModel.getListenEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void s) {
                openListen();
            }
        });

        mViewModel.getReadEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void s) {
                openRead();
            }
        });

        mViewModel.getReserveEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void s) {
                openReserve();
            }
        });

        mViewModel.getShopEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void s) {
                openShop();
            }
        });
    }

    @Override
    public void openExplore() {
        startActivity(new Intent(getActivity(), ExploreActivity.class));
    }

    @Override
    public void openListen() {
        startActivity(new Intent(getActivity(), ListenActivity.class));
    }

    @Override
    public void openShop() {
        startActivity(new Intent(getActivity(), ShopActivity.class));
    }

    @Override
    public void openRead() {
        startActivity(new Intent(getActivity(), BlogActivity.class));
    }

    @Override
    public void openReserve() {
        startActivity(new Intent(getActivity(), ReservationActivity.class));
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

}
